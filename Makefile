# I am a comment, and I want to say that the variable CC will be
# the compiler to use.
CC=gcc
INC_DIR = ./includes
OBJ_DIR = ./
CFLAGS=-c -Wall -I$(INC_DIR) -I./ -I./
#CXXFLAGS=-g $(shell root-config --cxx)
CPPFLAGS=-g $(shell root-config --cflags)
LDFLAGS=-g $(shell root-config --ldflags)
LDLIBS=$(shell root-config --libs
GLIBS  = $(shell root-config --glibs) -lRooFit -lRooFitCore -lstdc++ -lstdc++fs
BIN_DIR = ./
SRC_DIR = ./

OBJS=$(OBJ_DIR)/AtlasStyle.o $(OBJ_DIR)/AtlasUtils.o	$(OBJ_DIR)/RooGaussDoubleSidedExp.o  $(OBJ_DIR)/RooGaussDoubleSidedExpDict.o $(OBJ_DIR)/RooGaussExp.o  $(OBJ_DIR)/RooGaussExpDict.o  
STD_OBJ=$(OBJ_DIR)/AtlasStyle.o	$(OBJ_DIR)/AtlasUtils.o $(OBJ_DIR)/RooGaussDoubleSidedExp.o $(OBJ_DIR)/RooGaussDoubleSidedExpDict.o $(OBJ_DIR)/RooGaussExp.o $(OBJ_DIR)/RooGaussExpDict.o 
 
 
SOURCES1=$(subst build,$(SRC_DIR),$(STD_OBJ) )
SOURCES=$(subst .o,.cxx,$(SOURCES1) )

EXEC_SOURCES=TemplateFakeDev.cxx MakeTemplateData.cxx MakeABCDData.cxx ABCD.cxx

EXECUTABLE=TemplateFakeDevExe MakeTemplateDataExe MakeABCDDataExe ABCDExe

all: $(EXECUTABLE)

%Exe:$(OBJ_DIR)/%.o $(STD_OBJ) 
	$(CC) $(LDFLAGS) $(GLIBS) $(STD_OBJ) -std=c++17  $< -o $(BIN_DIR)/$@  -lstdc++fs 

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cxx
	@echo $(SOURCES)
	@echo $(STD_OBJ)
	@echo $(SOURCES1)
	$(CC) $(CFLAGS) $(CPPFLAGS) $< -o $@

$(OBJ_DIR)/%.o: %.cxx
	$(CC) $(CFLAGS) $(CPPFLAGS)  $< -o $@
	
depend: .depend

.depend:  $(EXEC_SOURCES) $(SOURCES)
	rm -f ./.depend
	$(CC) $(CFLAGS) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS) $(EXECUTABLE) 

dist-clean: clean
	$(RM) *~ .depend

include .depend

