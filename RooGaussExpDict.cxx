// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME RooGaussExpDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "RooGaussExp.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_RooGaussExp(void *p = 0);
   static void *newArray_RooGaussExp(Long_t size, void *p);
   static void delete_RooGaussExp(void *p);
   static void deleteArray_RooGaussExp(void *p);
   static void destruct_RooGaussExp(void *p);
   static void streamer_RooGaussExp(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooGaussExp*)
   {
      ::RooGaussExp *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooGaussExp >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooGaussExp", ::RooGaussExp::Class_Version(), "RooGaussExp.h", 13,
                  typeid(::RooGaussExp), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooGaussExp::Dictionary, isa_proxy, 16,
                  sizeof(::RooGaussExp) );
      instance.SetNew(&new_RooGaussExp);
      instance.SetNewArray(&newArray_RooGaussExp);
      instance.SetDelete(&delete_RooGaussExp);
      instance.SetDeleteArray(&deleteArray_RooGaussExp);
      instance.SetDestructor(&destruct_RooGaussExp);
      instance.SetStreamerFunc(&streamer_RooGaussExp);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooGaussExp*)
   {
      return GenerateInitInstanceLocal((::RooGaussExp*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RooGaussExp*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooGaussExp::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooGaussExp::Class_Name()
{
   return "RooGaussExp";
}

//______________________________________________________________________________
const char *RooGaussExp::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooGaussExp*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooGaussExp::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooGaussExp*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooGaussExp::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooGaussExp*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooGaussExp::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooGaussExp*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RooGaussExp::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooGaussExp.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      RooAbsPdf::Streamer(R__b);
      m.Streamer(R__b);
      m0.Streamer(R__b);
      sigma.Streamer(R__b);
      alpha.Streamer(R__b);
      R__b.CheckByteCount(R__s, R__c, RooGaussExp::IsA());
   } else {
      R__c = R__b.WriteVersion(RooGaussExp::IsA(), kTRUE);
      RooAbsPdf::Streamer(R__b);
      m.Streamer(R__b);
      m0.Streamer(R__b);
      sigma.Streamer(R__b);
      alpha.Streamer(R__b);
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooGaussExp(void *p) {
      return  p ? new(p) ::RooGaussExp : new ::RooGaussExp;
   }
   static void *newArray_RooGaussExp(Long_t nElements, void *p) {
      return p ? new(p) ::RooGaussExp[nElements] : new ::RooGaussExp[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooGaussExp(void *p) {
      delete ((::RooGaussExp*)p);
   }
   static void deleteArray_RooGaussExp(void *p) {
      delete [] ((::RooGaussExp*)p);
   }
   static void destruct_RooGaussExp(void *p) {
      typedef ::RooGaussExp current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooGaussExp(TBuffer &buf, void *obj) {
      ((::RooGaussExp*)obj)->::RooGaussExp::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooGaussExp

namespace {
  void TriggerDictionaryInitialization_RooGaussExpDict_Impl() {
    static const char* headers[] = {
"RooGaussExp.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.164/InstallArea/x86_64-centos7-gcc8-opt/include/",
"/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/includes/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "RooGaussExpDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate(R"ATTRDUMP(Your description goes here...)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$RooGaussExp.h")))  RooGaussExp;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "RooGaussExpDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "RooGaussExp.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"RooGaussExp", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RooGaussExpDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RooGaussExpDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RooGaussExpDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RooGaussExpDict() {
  TriggerDictionaryInitialization_RooGaussExpDict_Impl();
}
