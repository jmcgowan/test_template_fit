// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME RooGaussDoubleSidedExpDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "RooGaussDoubleSidedExp.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_RooGaussDoubleSidedExp(void *p = 0);
   static void *newArray_RooGaussDoubleSidedExp(Long_t size, void *p);
   static void delete_RooGaussDoubleSidedExp(void *p);
   static void deleteArray_RooGaussDoubleSidedExp(void *p);
   static void destruct_RooGaussDoubleSidedExp(void *p);
   static void streamer_RooGaussDoubleSidedExp(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooGaussDoubleSidedExp*)
   {
      ::RooGaussDoubleSidedExp *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooGaussDoubleSidedExp >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooGaussDoubleSidedExp", ::RooGaussDoubleSidedExp::Class_Version(), "RooGaussDoubleSidedExp.h", 13,
                  typeid(::RooGaussDoubleSidedExp), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooGaussDoubleSidedExp::Dictionary, isa_proxy, 16,
                  sizeof(::RooGaussDoubleSidedExp) );
      instance.SetNew(&new_RooGaussDoubleSidedExp);
      instance.SetNewArray(&newArray_RooGaussDoubleSidedExp);
      instance.SetDelete(&delete_RooGaussDoubleSidedExp);
      instance.SetDeleteArray(&deleteArray_RooGaussDoubleSidedExp);
      instance.SetDestructor(&destruct_RooGaussDoubleSidedExp);
      instance.SetStreamerFunc(&streamer_RooGaussDoubleSidedExp);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooGaussDoubleSidedExp*)
   {
      return GenerateInitInstanceLocal((::RooGaussDoubleSidedExp*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RooGaussDoubleSidedExp*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooGaussDoubleSidedExp::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooGaussDoubleSidedExp::Class_Name()
{
   return "RooGaussDoubleSidedExp";
}

//______________________________________________________________________________
const char *RooGaussDoubleSidedExp::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooGaussDoubleSidedExp*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooGaussDoubleSidedExp::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooGaussDoubleSidedExp*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooGaussDoubleSidedExp::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooGaussDoubleSidedExp*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooGaussDoubleSidedExp::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooGaussDoubleSidedExp*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RooGaussDoubleSidedExp::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooGaussDoubleSidedExp.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      RooAbsPdf::Streamer(R__b);
      m.Streamer(R__b);
      m0.Streamer(R__b);
      sigma.Streamer(R__b);
      alphaLo.Streamer(R__b);
      alphaHi.Streamer(R__b);
      R__b.CheckByteCount(R__s, R__c, RooGaussDoubleSidedExp::IsA());
   } else {
      R__c = R__b.WriteVersion(RooGaussDoubleSidedExp::IsA(), kTRUE);
      RooAbsPdf::Streamer(R__b);
      m.Streamer(R__b);
      m0.Streamer(R__b);
      sigma.Streamer(R__b);
      alphaLo.Streamer(R__b);
      alphaHi.Streamer(R__b);
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooGaussDoubleSidedExp(void *p) {
      return  p ? new(p) ::RooGaussDoubleSidedExp : new ::RooGaussDoubleSidedExp;
   }
   static void *newArray_RooGaussDoubleSidedExp(Long_t nElements, void *p) {
      return p ? new(p) ::RooGaussDoubleSidedExp[nElements] : new ::RooGaussDoubleSidedExp[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooGaussDoubleSidedExp(void *p) {
      delete ((::RooGaussDoubleSidedExp*)p);
   }
   static void deleteArray_RooGaussDoubleSidedExp(void *p) {
      delete [] ((::RooGaussDoubleSidedExp*)p);
   }
   static void destruct_RooGaussDoubleSidedExp(void *p) {
      typedef ::RooGaussDoubleSidedExp current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooGaussDoubleSidedExp(TBuffer &buf, void *obj) {
      ((::RooGaussDoubleSidedExp*)obj)->::RooGaussDoubleSidedExp::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooGaussDoubleSidedExp

namespace {
  void TriggerDictionaryInitialization_RooGaussDoubleSidedExpDict_Impl() {
    static const char* headers[] = {
"RooGaussDoubleSidedExp.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.164/InstallArea/x86_64-centos7-gcc8-opt/include/",
"/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/includes/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "RooGaussDoubleSidedExpDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate(R"ATTRDUMP(Your description goes here...)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$RooGaussDoubleSidedExp.h")))  RooGaussDoubleSidedExp;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "RooGaussDoubleSidedExpDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "RooGaussDoubleSidedExp.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"RooGaussDoubleSidedExp", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RooGaussDoubleSidedExpDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RooGaussDoubleSidedExpDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RooGaussDoubleSidedExpDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RooGaussDoubleSidedExpDict() {
  TriggerDictionaryInitialization_RooGaussDoubleSidedExpDict_Impl();
}
