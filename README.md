Quick test of loose fits for jets faking photon estimates

Consists of Gaussian with exponential tails fit to leakage shape, then combined leakage and non-tight photon shape fit to data.

Data and asimov inputs can be found in /eos/user/j/jmcgowan/test_fit_data/


To setup and run:

```
git clone ssh://git@gitlab.cern.ch:7999/jmcgowan/test_template_fit.git
cd test_template_fit
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup 22.2.54,AnalysisBase
make
./TemplateFakeDevExe path_to_input legendnote1 legendnote2
# For example
./TemplateFakeDevExe /eos/user/j/jmcgowan/test_fit_data/fakes_nn_data_1201/ LP5 LP5
```
