// C++ and ROOT includeds
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <map>
#include <stdio.h>
#include<cmath>
#include "/afs/cern.ch/user/j/jmcgowan/tdrstyle.C"

#include "TFile.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TH1.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TChain.h"
#include<TRandom.h>
#include "TLorentzVector.h"
#include "TEnv.h"

// RooFit includes
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooChebychev.h"
#include "RooFitResult.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "RooWorkspace.h"
#include "RooProdPdf.h"
#include "RooCBShape.h"
#include "RooBukinPdf.h"
#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooExponential.h"
#include "RooNovosibirsk.h"
#include "RooLandau.h"
#include "RooGExpModel.h"
#include "RooJohnson.h"
#include "RooKeysPdf.h"

using namespace std;
using namespace RooFit;

#include "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/utils.C"



// GLOBAL VARIABLES

//channel - choose "electron", "muon" or "combined"
string channel;

bool debug;
bool mc_closure;

//GLOBAL variables
string outfile;
string histofile;
string workspace;
string region;
int lp_def;

//input data
vector<string> dirList = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/data"
                          };
vector<string> fileList_data = return_sample_list(dirList);

// Input mc
vector<string> dirList_signal_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Signal"
                                   };
vector<string> fileList_signal_mc = return_sample_list(dirList_signal_mc);
vector<string> dirList_qcd_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/QCD"
                                 };
vector<string> fileList_qcd_mc = return_sample_list(dirList_qcd_mc);

vector<string> dirList_qcd_mg_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/QCD_Mgraph"
                                 };
vector<string> fileList_qcd_mg_mc = return_sample_list(dirList_qcd_mg_mc);

vector<string> dirList_wjets_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/Wjets"
                                   };
vector<string> fileList_wjets_mc = return_sample_list(dirList_wjets_mc);
vector<string> dirList_zjets_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/Zjets"
                                   };
vector<string> fileList_zjets_mc = return_sample_list(dirList_zjets_mc);

vector<string> dirList_ttbar_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/ttbar"};
vector<string> fileList_ttbar_mc = return_sample_list(dirList_ttbar_mc);

vector<string> dirList_photonjets_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Photonjets"};
vector<string> fileList_photonjets_mc = return_sample_list(dirList_photonjets_mc);

vector<string> dirList_singletop_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/SingleTop"};
vector<string> fileList_singletop_mc = return_sample_list(dirList_singletop_mc);

vector<string> dirList_tWgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/tWgamma"};
vector<string> fileList_tWgamma_mc = return_sample_list(dirList_tWgamma_mc);

vector<string> dirList_ttgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/ttgamma"};
vector<string> fileList_ttgamma_mc = return_sample_list(dirList_ttgamma_mc);

vector<string> dirList_zgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/Zgamma"};
vector<string> fileList_zgamma_mc = return_sample_list(dirList_zgamma_mc);

vector<string> dirList_ewkzgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/EWKZgamma"};
vector<string> fileList_ewkzgamma_mc = return_sample_list(dirList_ewkzgamma_mc);

vector<string> dirList_WVBF_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/WVBF"};
vector<string> fileList_WVBF_mc = return_sample_list(dirList_WVBF_mc);

vector<string> dirList_ZVBF_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_08132021/MC/Other/ZVBF"};
vector<string> fileList_ZVBF_mc = return_sample_list(dirList_ZVBF_mc);


//ANALYSIS BINS
//const Int_t NBINS = 6;
//Double_t edges[NBINS+1] = {500,544,603,688,809,1004,2328};
const Int_t NBINS = 5;
//Double_t edges[NBINS+1] = {500,544,603,688,809,1004,2328};
Double_t edges[NBINS+1] = {0,.2,.4,.6,.8,1.0};
//const Int_t NBINS = 12;
//double pi = TMath::Pi();
//Double_t edges[NBINS+1] = {-pi,-pi*(15.0/16.0),-pi*(7.0/8.0),-pi*(3.0/4.0),-pi*(1.0/2.0),-pi *(1.0/4.0),0,pi*(1.0/4.0),pi*(1.0/2.0),pi*(3.0/4.0),pi*(7.0/8.0),pi*(15.0/16.0),pi};
//const Int_t NBINS = 6;
//Double_t edges[NBINS+1] = {100,140,200,275,400,550,1050};
//
//const Int_t NBINS = 5;
//Double_t edges[NBINS+1] = {0,2,3,4,5,9};

RooDataSet *raw_gam_temp[NBINS], *raw_jet_temp[NBINS], *raw_nt_jet_mc[NBINS], *raw_signal[NBINS], *raw_tight_signal[NBINS], *raw_loose_signal[NBINS], *raw_leakage[NBINS], *w_gam_temp[NBINS], *w_jet_temp[NBINS], *w_signal[NBINS], *w_loose_signal[NBINS],*w_tight_signal[NBINS], *w_leakage[NBINS], *w_nt_jet_mc[NBINS];
RooDataSet *set_sig_temp[NBINS];
RooDataSet *raw_leakage_combined, *w_leakage_combined;
RooRealVar *w, *var_iso;
RooWorkspace *wkspace;

int get_bin(double num){
  for (int i = 0; i < NBINS; i++){
    if (num < edges[i+1]){
      return i;
    }
  }
  return NBINS-1;
}

void read_sample(vector<string> fileList, bool isData)
{
  TFile *fin = new TFile(histofile.c_str(),"update");
  if (fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  //Declare variables for use inside loop
  bool truth_photon, truth_hadron;
  double iso, sumofweights, xs, lumi, temp_weight, weight, variable;
  int bin;

  // Get histos
  TH1F* signal_histos_cr[NBINS];
  TH1F* template_histos_cr[NBINS];
  TH1F* histos_vr[NBINS];
  TH1F* histos_leak[NBINS];
  TTree *trees_sig_temp[NBINS], *trees_bkg_temp[NBINS], *trees_sig[NBINS], *trees_leak[NBINS];
  TH1F* h_leak;  
  for (int i = 0; i < NBINS; i++){
    fin->cd();
    signal_histos_cr[i] = (TH1F*)fin->Get(("h_iso_sig_cr"+to_string(i)).c_str());
    template_histos_cr[i] = (TH1F*)fin->Get(("h_iso_temp_cr"+to_string(i)).c_str());
    histos_vr[i] = (TH1F*)fin->Get(("h_iso_vr"+to_string(i)).c_str());
    histos_leak[i] = (TH1F*)fin->Get(("h_iso_leak"+to_string(i)).c_str());
    trees_sig_temp[i] = (TTree*)fin->Get(("tree_sig_temp"+to_string(i)).c_str());
    trees_bkg_temp[i] = (TTree*)fin->Get(("tree_bkg_temp"+to_string(i)).c_str());
    trees_sig[i] = (TTree*)fin->Get(("tree_sig"+to_string(i)).c_str());
    trees_leak[i] = (TTree*)fin->Get(("tree_leak"+to_string(i)).c_str());
    trees_sig_temp[i]->SetBranchAddress("iso",&iso);
    trees_bkg_temp[i]->SetBranchAddress("iso",&iso);
    trees_sig[i]->SetBranchAddress("iso",&iso);
    trees_leak[i]->SetBranchAddress("iso",&iso);
    trees_sig_temp[i]->SetBranchAddress("weight",&weight);
    trees_bkg_temp[i]->SetBranchAddress("weight",&weight);
    trees_sig[i]->SetBranchAddress("weight",&weight);
    trees_leak[i]->SetBranchAddress("weight",&weight);
  }
  h_leak = (TH1F*)fin->Get("h_leakage");

  const int nFiles = fileList.size();
  vector<string> subfileList;
  for (int i = 0; i < nFiles; i++){
    subfileList = return_file_list({fileList.at(i)});
    TTreeReader *vbswy_treeReader;
    TTreeReader *DAOD_treeReader;
    TChain vbswy_chain("vbswy");
    TChain DAOD_chain("DAOD_tree");
    for (const auto fName: subfileList){
      TFile *f = new TFile(("root://jmcgowan.cern.ch/"+fName).c_str(),"READ");
      if(!f->IsOpen()){
        throw runtime_error("Unable to open file");
      }
      vbswy_chain.Add(fName.c_str());
      DAOD_chain.Add(fName.c_str());
      f->Close();
    }

    vbswy_treeReader = new TTreeReader(&vbswy_chain);
    DAOD_treeReader = new TTreeReader(&DAOD_chain);

    TTreeReaderValue<double> nn_score(*vbswy_treeReader, "nn_score");
    TTreeReaderValue<int> gam_n(*vbswy_treeReader, "gam_n");
    TTreeReaderValue<int> j_n(*vbswy_treeReader, "j_n");
    TTreeReaderValue<int> lep_n(*vbswy_treeReader, "lep_n");
    TTreeReaderValue<int> pass_ly_Z_veto(*vbswy_treeReader,"pass_ly_Z_veto");
    TTreeReaderValue<int> pass_second_lepton_veto(*vbswy_treeReader,"pass_second_lepton_veto");
    TTreeReaderValue<double> weight_all(*vbswy_treeReader, "weight_all");
    TTreeReaderValue<double> weight_mc(*vbswy_treeReader, "weight_mc");
    TTreeReaderValue<double> jj_m(*vbswy_treeReader, "jj_m");
    TTreeReaderValue<double> jj_drap(*vbswy_treeReader, "jj_drap");
    TTreeReaderValue<double> w_mt(*vbswy_treeReader, "w_mt");
    TTreeReaderValue<double> met(*vbswy_treeReader, "met");
    TTreeReaderValue<double> j0_pt(*vbswy_treeReader, "j0_pt");
    TTreeReaderValue<double> j1_pt(*vbswy_treeReader, "j1_pt");
    TTreeReaderValue<double> gam_pt(*vbswy_treeReader, "gam_pt");
    TTreeReaderValue<double> gam_eta(*vbswy_treeReader, "gam_eta");
    TTreeReaderValue<double> lep_pt(*vbswy_treeReader, "lep_pt");
    TTreeReaderValue<double> lep_eta(*vbswy_treeReader, "lep_eta");
    TTreeReaderValue<double> e_isoTight(*vbswy_treeReader,"e_isoTight");
    TTreeReaderValue<double> e_idTight(*vbswy_treeReader,"e_idTight");
    TTreeReaderValue<bool> e_passIP(*vbswy_treeReader,"e_passIP");
    TTreeReaderValue<bool> mu_idMedium(*vbswy_treeReader,"mu_idMedium");
    TTreeReaderValue<bool> mu_idTight(*vbswy_treeReader,"mu_idTight");
    TTreeReaderValue<bool> mu_isoFCTight(*vbswy_treeReader,"mu_isoFCTight");
    TTreeReaderValue<bool> mu_passIP(*vbswy_treeReader,"mu_passIP");
    TTreeReaderValue<double> gam_topoETcone40(*vbswy_treeReader, "gam_topoETcone40");
    TTreeReaderValue<double> gam_topoETcone20(*vbswy_treeReader, "gam_topoETcone20");
    TTreeReaderValue<double> gam_pTcone20(*vbswy_treeReader, "gam_pTcone20");
    TTreeReaderValue<bool> gam_isLoosePrime2(*vbswy_treeReader, "gam_isLoosePrime2");
    TTreeReaderValue<bool> gam_isLoosePrime3(*vbswy_treeReader, "gam_isLoosePrime3");
    TTreeReaderValue<bool> gam_idLoose(*vbswy_treeReader, "gam_idLoose");
    TTreeReaderValue<bool> gam_isLoosePrime4a(*vbswy_treeReader, "gam_isLoosePrime4a");
    TTreeReaderValue<bool> gam_isLoosePrime5(*vbswy_treeReader, "gam_isLoosePrime5");
    TTreeReaderValue<bool> gam_idTight(*vbswy_treeReader, "gam_idTight");
    TTreeReaderValue<int> pass_dq(*vbswy_treeReader, "pass_dq");
    TTreeReaderValue<int> pass_jetclean(*vbswy_treeReader, "pass_jetclean");
    TTreeReaderValue<int> pass_onelep(*vbswy_treeReader,"pass_onelep");
    TTreeReaderValue<int> pass_onegam(*vbswy_treeReader,"pass_onegam");
    TTreeReaderValue<int> pass_twojet(*vbswy_treeReader,"pass_twojet");
    TTreeReaderValue<int> pass_trigger(*vbswy_treeReader,"pass_trigger");
    TTreeReaderValue<int> pass_primary_vertex(*vbswy_treeReader, "pass_primary_vertex");
    TTreeReaderValue<int> is_Wenu(*vbswy_treeReader, "is_Wenu");
    TTreeReaderValue<int> is_Wmunu(*vbswy_treeReader, "is_Wmunu");
    TTreeReaderValue<int> gam_truth_type(*vbswy_treeReader,"gam_truth_type");
    TTreeReaderValue<int> gam_truth_origin(*vbswy_treeReader,"gam_truth_origin");
    TTreeReaderValue<bool> pass_vy_OR(*vbswy_treeReader,"pass_vy_OR");
    TTreeReaderValue<double> weight_m(*vbswy_treeReader, "weight_m");
    TTreeReaderValue<double> weight_e(*vbswy_treeReader, "weight_e");
    TTreeReaderValue<int> n_bjets_85(*vbswy_treeReader, "n_bjets_85");
    TTreeReaderValue<unsigned int> dsid(*vbswy_treeReader,"dsid");
    TTreeReaderValue<int> Wy2jEventType(*vbswy_treeReader, "Wy2jEventType");
    TTreeReaderValue<int> pass_dr(*vbswy_treeReader, "pass_dr");    
    TTreeReaderValue<int> pass_dphi(*vbswy_treeReader, "pass_dphi");
    TTreeReaderValue<int> pass_sr_wy(*vbswy_treeReader, "pass_sr_wy");
    TTreeReaderValue<int> pass_cra_wy(*vbswy_treeReader, "pass_cra_wy");
    TTreeReaderValue<int> pass_crb_wy(*vbswy_treeReader, "pass_crb_wy");
    TTreeReaderValue<int> pass_crc_wy(*vbswy_treeReader, "pass_crc_wy");
    TTreeReaderValue<int> n_gapjets(*vbswy_treeReader, "n_gapjets");
    TTreeReaderValue<double> ly_xi(*vbswy_treeReader, "ly_xi");
    TTreeReaderValue<double> wy_xi(*vbswy_treeReader, "wy_xi");
    TTreeReaderValue<double> jj_dphi_signed(*vbswy_treeReader, "jj_dphi_signed");
    TTreeReaderValue<double> jj_pt(*vbswy_treeReader, "jj_pt");

    //Values from DAOD tree
    TTreeReaderValue<double> luminosity(*DAOD_treeReader, "luminosity");
    TTreeReaderValue<double> xsection(*DAOD_treeReader, "xsection");
    TTreeReaderValue<double> sum_of_weights(*DAOD_treeReader, "sum_of_weights");
    TTreeReaderValue<double> kfactor(*DAOD_treeReader, "kfactor");
    TTreeReaderValue<double> geneff(*DAOD_treeReader, "geneff");

    DAOD_treeReader->Next();
    sumofweights = *sum_of_weights;
    xs = *xsection * (*geneff) * (*kfactor);
    lumi = *luminosity;
    while(DAOD_treeReader->Next()){
      sumofweights += *sum_of_weights;
    }
    if (isData && !mc_closure){
      xs = 1.0;
      lumi = 1.0;
      sumofweights = 1.0;
    }
    int counter = 0;
    while (vbswy_treeReader->Next()){
      counter++;
      if (debug && counter == 100){
        break;
      }
      weight = (*weight_e) * (*weight_m) * (*weight_all) * xs * lumi /sumofweights;
      if ((*dsid >= 700015 && *dsid <= 700017) || (*dsid >= 504678 && *dsid <= 504680)){
        weight = .5 * weight; //half weight for QCD samples, to take average of two QCD generators
      } 

      //Make selection
      bool signal_selec = ((*dsid > 363272) || (*dsid < 363270)) || (*Wy2jEventType > 0 && *Wy2jEventType < 3);
      bool pass_baseline_e = ( (*pass_onegam)&&(*pass_trigger)&&(*pass_twojet)&&(*pass_primary_vertex)&&
                      (*pass_dq)&&(*pass_jetclean)&&(*pass_vy_OR)&&(*is_Wenu)&&(*pass_onelep)&&
                      (*e_passIP)&&(*e_isoTight==1.0)&&(*e_idTight==1.0)&&
                      (*pass_second_lepton_veto)&&(*gam_pt>22.0)&&(*lep_pt > 30.0)&&(*met > 30.0)&&
                      (*lep_eta > -2.5)&&(*lep_eta < 2.5)&&(*gam_eta > -2.37)&&(*gam_eta < 2.37)&&
                      (*w_mt > 30.0)&&(*pass_ly_Z_veto)&&(*pass_dphi)&&(*pass_dr)&&
                      (*j0_pt > 50.0)&&(*j1_pt > 50.0)&&(*n_bjets_85 == 0) );

      bool pass_baseline_mu = ( (*pass_onegam)&&(*pass_trigger)&&(*pass_twojet)&&(*pass_primary_vertex)&&
                       (*pass_dq)&&(*pass_jetclean)&&(*pass_vy_OR)&&(*is_Wmunu)&&(*pass_onelep)&&
                       (*mu_passIP)&&(*mu_isoFCTight==1.0)&&(*mu_idTight==1.0)&&
                       (*pass_second_lepton_veto)&&(*gam_pt>22.0)&&(*lep_pt > 30.0)&&(*met > 30.0)&&
                       (*lep_eta > -2.5)&&(*lep_eta < 2.5)&&(*gam_eta > -2.37)&&(*gam_eta < 2.37)&&
                       (*w_mt > 30.0)&&(*pass_ly_Z_veto)&&(*pass_dphi)&&(*pass_dr)&&
                       (*j0_pt > 50.0)&&(*j1_pt > 50.0)&&(*n_bjets_85 == 0) );
      bool selection;
      if (channel == "combined"){
        selection = (pass_baseline_e || pass_baseline_mu);
      }
      else{
        selection = ( (channel == "electron") ? pass_baseline_e : pass_baseline_mu);
      }
      selection = selection && signal_selec && (*jj_drap > 2) && (*jj_m > 500);

     if (region == "sr"){
        selection = selection && (*n_gapjets==0) && (*wy_xi <= 0.35);
      }
      if (region == "cr1"){
        selection = selection && (*n_gapjets>=1) && (*wy_xi <= 0.35);
      }
      if (region == "cr2"){
        selection = selection && (*n_gapjets>=1) && (*wy_xi > 0.35);
      }
      if (region == "cr3"){
        selection = selection && (*n_gapjets==0) && (*wy_xi <= 0.35);
      }
      selection = selection && (weight > -10);

      truth_photon = (get_truth_type(*gam_truth_type,*gam_truth_origin) == "photon");
      truth_hadron = (get_truth_type(*gam_truth_type,*gam_truth_origin) == "hadron");
      variable = (*nn_score); // Variable to perform estimate in
      selection = selection && (variable > edges[0]); // Make sure variable is at least at minimum bin 
      bool isLoose=0;
      if (lp_def == 2){
        isLoose = *gam_isLoosePrime2;
      }
      else if (lp_def == 3){
        isLoose = *gam_isLoosePrime3;
      }
      else if (lp_def == 4){
        isLoose = *gam_idLoose;
      }
      else if (lp_def == 5){
        isLoose = *gam_isLoosePrime5;
      }
      if (selection){
        bin = get_bin(variable);
        iso = ((*gam_topoETcone40)) - (.022 * (*gam_pt));
	*var_iso = iso;
        if (*gam_idTight){
          if(!isData && truth_photon){
            signal_histos_cr[bin]->Fill(iso,weight);
            trees_sig_temp[bin]->Fill();
	    w_gam_temp[bin]->add(RooArgSet(*var_iso),weight);
          }
          else if (isData){
            trees_sig[bin]->Fill();
            histos_vr[bin]->Fill(iso,weight);
            w_signal[bin]->add(RooArgSet(*var_iso),weight);
            if (mc_closure && truth_photon){
              w_tight_signal[bin]->add(RooArgSet(*var_iso),weight);
            }
            else if (mc_closure && truth_hadron){
              w_loose_signal[bin]->add(RooArgSet(*var_iso),weight);
            }
          }
        }
        else if ((isLoose) && (!(*gam_idTight))){
          if (mc_closure && !isData && truth_hadron){
            w_nt_jet_mc[bin]->add(RooArgSet(*var_iso),weight);
          }
          if (isData){
            template_histos_cr[bin]->Fill(iso,weight);
            trees_bkg_temp[bin]->Fill();
            w_jet_temp[bin]->add(RooArgSet(*var_iso),weight);
          }
          else if (!isData && truth_photon){
            histos_leak[bin]->Fill(iso,weight);
            trees_leak[bin]->Fill();
            w_leakage_combined->add(RooArgSet(*var_iso),weight);
            w_leakage[bin]->add(RooArgSet(*var_iso),weight);
            if (variable > edges[NBINS]){
              h_leak->AddBinContent(NBINS,weight);
            }
            else{
              h_leak->Fill(variable,weight);
            }
          }
        }
      }
    } //end of loop over events
    delete vbswy_treeReader;
    delete DAOD_treeReader;
  } // end of loop over files
  fin->cd();
  h_leak->Write(0,TObject::kWriteDelete);
  delete h_leak;
  for (int i = 0; i < NBINS; i++){     
    signal_histos_cr[i]->Write(0,TObject::kWriteDelete);
    template_histos_cr[i]->Write(0,TObject::kWriteDelete);
    histos_vr[i]->Write(0,TObject::kWriteDelete);
    histos_leak[i]->Write(0,TObject::kWriteDelete);
    delete signal_histos_cr[i];
    delete template_histos_cr[i];
    delete histos_vr[i];
    delete histos_leak[i];
    trees_sig_temp[i]->Write(0,TObject::kWriteDelete);
    trees_bkg_temp[i]->Write(0,TObject::kWriteDelete);
    trees_sig[i]->Write(0,TObject::kWriteDelete);
    trees_leak[i]->Write(0,TObject::kWriteDelete);
    delete trees_sig_temp[i];
    delete trees_bkg_temp[i];
    delete trees_sig[i];
    delete trees_leak[i];
  }
  fin->Close();
  delete fin;

  return;
}

// Initialize trees
void initialize()
{
  TFile *fout = new TFile(histofile.c_str(),"update");
  if (fout == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }
  

  wkspace = new RooWorkspace("ws");

  fout->cd();
  TH1F *h_iso_sig_cr, *h_iso_temp_cr, *h_iso_vr, *h_iso_leak, *h_leakage;
  double iso, weight;
  TTree *tree_sig_temp, *tree_bkg_temp, *tree_sig, *tree_leak;
  //leakage histo
  h_leakage = new TH1F("h_leakage","h_leakage",NBINS,edges);
  h_leakage->Write(0,TObject::kWriteDelete);
  delete h_leakage;
  for (int i = 0; i < NBINS; i++){
    h_iso_sig_cr = new TH1F(("h_iso_sig_cr"+to_string(i)).c_str(),";E_{T}cone40 - 0.022pT(#gamma);",40,-25,55);
    h_iso_temp_cr = new TH1F(("h_iso_temp_cr"+to_string(i)).c_str(),";E_{T}cone40 - 0.022pT(#gamma);",40,-25,55);
    h_iso_vr = new TH1F(("h_iso_vr"+to_string(i)).c_str(),";E_{T}cone40 - 0.022pT(#gamma);",40,-25,55);
    h_iso_leak = new TH1F(("h_iso_leak"+to_string(i)).c_str(),";E_{T}cone40 - 0.022pT(#gamma);",40,-25,55);
    h_iso_sig_cr->Write(0,TObject::kWriteDelete);
    h_iso_temp_cr->Write(0,TObject::kWriteDelete);
    h_iso_vr->Write(0,TObject::kWriteDelete);
    h_iso_leak->Write(0,TObject::kWriteDelete);
    tree_sig_temp = new TTree(("tree_sig_temp"+to_string(i)).c_str(),("Signal template data "+to_string(i)).c_str());
    tree_bkg_temp = new TTree(("tree_bkg_temp"+to_string(i)).c_str(),("Background template data "+to_string(i)).c_str());
    tree_sig = new TTree(("tree_sig"+to_string(i)).c_str(),("Signal data "+to_string(i)).c_str());
    tree_leak = new TTree(("tree_leak"+to_string(i)).c_str(),("Leakage template data"+to_string(i)).c_str());
    tree_sig_temp->Branch("iso",&iso);
    tree_sig_temp->Branch("weight",&weight);
    tree_bkg_temp->Branch("iso",&iso);
    tree_bkg_temp->Branch("weight",&weight);
    tree_sig->Branch("iso",&iso);
    tree_sig->Branch("weight",&weight);
    tree_leak->Branch("iso",&iso);
    tree_leak->Branch("weight",&weight);
    tree_sig_temp->Write(0,TObject::kWriteDelete);
    tree_bkg_temp->Write(0,TObject::kWriteDelete);
    tree_sig->Write(0,TObject::kWriteDelete);
    tree_leak->Write(0,TObject::kWriteDelete);
    var_iso = new RooRealVar("var_iso","E_{T}cone40 - 0.022pT(#gamma)",-25,55);
    w = new RooRealVar("w","w",-50,50);
    raw_gam_temp[i] = new RooDataSet(("gam_temp"+to_string(i)).c_str(),("gam_temp"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
    w_gam_temp[i] = new RooDataSet(raw_gam_temp[i]->GetName(),raw_gam_temp[i]->GetTitle(),raw_gam_temp[i], *raw_gam_temp[i]->get(),0,w->GetName());
    raw_jet_temp[i] = new RooDataSet(("jet_temp"+to_string(i)).c_str(),("jet_temp"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
    w_jet_temp[i] = new RooDataSet(raw_jet_temp[i]->GetName(),raw_jet_temp[i]->GetTitle(),raw_jet_temp[i], *raw_jet_temp[i]->get(),0,w->GetName());
    raw_signal[i] = new RooDataSet(("signal"+to_string(i)).c_str(),("signal"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
    w_signal[i] = new RooDataSet(raw_signal[i]->GetName(),raw_signal[i]->GetTitle(),raw_signal[i], *raw_signal[i]->get(),0,w->GetName());
    raw_leakage[i] = new RooDataSet(("leakage"+to_string(i)).c_str(),("leakage"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
    w_leakage[i] = new RooDataSet(raw_leakage[i]->GetName(),raw_leakage[i]->GetTitle(),raw_leakage[i], *raw_leakage[i]->get(),0,w->GetName());
    if (mc_closure){
      raw_nt_jet_mc[i] = new RooDataSet(("nt_jet_mc"+to_string(i)).c_str(),("nt_jet_mc"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
      w_nt_jet_mc[i] = new RooDataSet(raw_nt_jet_mc[i]->GetName(),raw_nt_jet_mc[i]->GetTitle(),raw_nt_jet_mc[i], *raw_nt_jet_mc[i]->get(),0,w->GetName());
      raw_tight_signal[i] = new RooDataSet(("tight_signal"+to_string(i)).c_str(),("tight_signal"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
      w_tight_signal[i] = new RooDataSet(raw_tight_signal[i]->GetName(),raw_tight_signal[i]->GetTitle(),raw_tight_signal[i], *raw_tight_signal[i]->get(),0,w->GetName());
      raw_loose_signal[i] = new RooDataSet(("loose_signal"+to_string(i)).c_str(),("loose_signal"+to_string(i)).c_str(),RooArgSet(*var_iso,*w));
      w_loose_signal[i] = new RooDataSet(raw_loose_signal[i]->GetName(),raw_loose_signal[i]->GetTitle(),raw_loose_signal[i], *raw_loose_signal[i]->get(),0,w->GetName());
    }
    delete tree_sig_temp;
    delete tree_bkg_temp;
    delete tree_sig;
    delete tree_leak;
    delete h_iso_sig_cr;
    delete h_iso_temp_cr;
    delete h_iso_vr;
    delete h_iso_leak;
  }
  raw_leakage_combined = new RooDataSet("leakage_combined","leakage_combined",RooArgSet(*var_iso,*w));
  w_leakage_combined = new RooDataSet(raw_leakage_combined->GetName(),raw_leakage_combined->GetTitle(),raw_leakage_combined, *raw_leakage_combined->get(),0,w->GetName());
  fout->Close();
  delete fout;
}

void read_config(const char *inputConfig)
{
  TEnv config;

  config.ReadFile(inputConfig, EEnvLevel(0));

  outfile = config.GetValue("directory","None");

  channel = config.GetValue("channel","combined");

  lp_def = config.GetValue("Loose_def",4);
  region = config.GetValue("Region","None");

  mc_closure = config.GetValue("MC_Closure",0);

  debug = config.GetValue("Debug",0);

  config.Print();
}

// Main
int main(int argc, char **argv)
{
  if (argc < 2){
    cout<<"Invalid call to MakeTemplateDataExe"<<endl;
    cout<<"Valid call:"<<endl;
    cout<<"./MakeTemplateDataExe configfile.config"<<endl;
    return 1;
  }

  const char *inputConfig = argv[1];
  read_config(inputConfig);

  histofile = outfile+"/template_data_MC.root";
  workspace = outfile+"/workspace_data_MC.root";

    
  initialize();
  if (mc_closure){
    read_sample(fileList_signal_mc,1);
    read_sample(fileList_qcd_mc,1);
    read_sample(fileList_qcd_mg_mc,1);
    read_sample(fileList_wjets_mc,1);
    read_sample(fileList_zjets_mc,1);
    read_sample(fileList_ttbar_mc,1);
    read_sample(fileList_ttgamma_mc,1);
    read_sample(fileList_singletop_mc,1);
    read_sample(fileList_tWgamma_mc,1);
    read_sample(fileList_WVBF_mc,1);
    read_sample(fileList_ZVBF_mc,1);
    read_sample(fileList_zgamma_mc,1);
    read_sample(fileList_ewkzgamma_mc,1);
  } else{
    read_sample(fileList_data,1);
  }

  read_sample(fileList_signal_mc,0);
  read_sample(fileList_qcd_mc,0);
  read_sample(fileList_qcd_mg_mc,0);
  read_sample(fileList_wjets_mc,0);
  read_sample(fileList_zjets_mc,0);
  read_sample(fileList_ttbar_mc,0);
  read_sample(fileList_ttgamma_mc,0);
  read_sample(fileList_singletop_mc,0);
  read_sample(fileList_tWgamma_mc,0);
  read_sample(fileList_WVBF_mc,0);
  read_sample(fileList_ZVBF_mc,0);
  read_sample(fileList_zgamma_mc,0);
  read_sample(fileList_ewkzgamma_mc,0);

  for (int i = 0; i < NBINS; i++){
    wkspace->import(*w_gam_temp[i]);
    wkspace->import(*w_jet_temp[i]);
    wkspace->import(*w_signal[i]);
    wkspace->import(*w_leakage[i]);
    if (mc_closure){
      wkspace->import(*w_tight_signal[i]);
      wkspace->import(*w_loose_signal[i]);
      wkspace->import(*w_nt_jet_mc[i]);
    }
  }
  wkspace->import(*w_leakage_combined);
  wkspace->writeToFile(workspace.c_str(),kTRUE);

}

