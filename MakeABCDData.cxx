#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <map>
#include "TFile.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TH1.h"
#include <stdio.h>
#include<cmath>

#include "TFile.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TH1.h"
#include "TTree.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TTreeReader.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TChain.h"
#include<TRandom.h>
#include "TLorentzVector.h"

using namespace std;
 
#include "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/utils.C"


// GLOBAL VARIABLES

//DEBUG: run on 1000 events only
bool debug = false;
bool mc_closure;
string region;
int lp_def;
string outfile;
string output_file;

// value of Eiso gap
int gapValue = 3;

//channel - choose "electron", "muon" or "combined"
string channel = "combined";

//histogram output
//string output_file = "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/data_02042021/ABCD_mjj/ABCD.root";

//pdf output
//string outputpdf = "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/data_02042021/ABCD_mjj/ABCD.pdf";

//text output
//string output_txt = "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/data_02042021/ABCD_mjj/ABCD.txt";

// Input data
vector<string> dirList = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/DATA/DATA15_16"
                          };
vector<string> fileList_data = return_sample_list(dirList);

//vector<string> dirList_MC_closure = {"/eos/user/j/jmcgowan/Public/NTUPLES_1230/MC"
//                          };
//vector<string> fileList_MC_closure = return_sample_list(dirList_MC_closure);


// Input mc
vector<string> dirList_signal_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Signal/"
                                   };
vector<string> fileList_signal_mc = return_sample_list(dirList_signal_mc);
vector<string> dirList_qcd_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/QCD_Mgraph"
                                 };
vector<string> fileList_qcd_mc = return_sample_list(dirList_qcd_mc);
vector<string> dirList_wjets_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/Wjets"
                                   };
vector<string> fileList_wjets_mc = return_sample_list(dirList_wjets_mc);
vector<string> dirList_zjets_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/Zjets"
                                   };
vector<string> fileList_zjets_mc = return_sample_list(dirList_zjets_mc);

vector<string> dirList_ttbar_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/ttbar"};
vector<string> fileList_ttbar_mc = return_sample_list(dirList_ttbar_mc);

vector<string> dirList_photonjets_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Photonjets"};
vector<string> fileList_photonjets_mc = return_sample_list(dirList_photonjets_mc);

vector<string> dirList_singletop_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/SingleTop"};
vector<string> fileList_singletop_mc = return_sample_list(dirList_singletop_mc);

vector<string> dirList_tWgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/tWgamma"};
vector<string> fileList_tWgamma_mc = return_sample_list(dirList_tWgamma_mc);

vector<string> dirList_ttgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/ttgamma"};
vector<string> fileList_ttgamma_mc = return_sample_list(dirList_ttgamma_mc);

vector<string> dirList_zgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/Zgamma"};
vector<string> fileList_zgamma_mc = return_sample_list(dirList_zgamma_mc);

vector<string> dirList_ewkzgamma_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/EWKZgamma"};
vector<string> fileList_ewkzgamma_mc = return_sample_list(dirList_ewkzgamma_mc);

vector<string> dirList_WVBF_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/WVBF"};
vector<string> fileList_WVBF_mc = return_sample_list(dirList_WVBF_mc);

vector<string> dirList_ZVBF_mc = {"/eos/user/j/jmcgowan/Public/NTUPLES_V07/MC16A/Other/ZVBF"};
vector<string> fileList_ZVBF_mc = return_sample_list(dirList_ZVBF_mc);


string var = "var";
string label = "NN_score";
const Int_t NBINS = 5;
Double_t edges[NBINS+1] = {0,0.2,0.4,0.6,0.8,1.0};


void readTrees(vector<string> fileList, bool isData, bool isMCClosure, string name)
{

  //Declare variables for use inside loop
  bool truth_photon, truth_hadron;
  double iso, sumofweights, xs, lumi, temp_weight, weight, variable;
  int bin;

  gEnv->SetValue("Root.Stacktrace","no");
  gSystem->ResetSignal(kSigSegmentationViolation, kTRUE);
  // open histograms to update
  TFile *fin = new TFile(output_file.c_str(), "Update");
  fin->cd();
  TH1F *h_var_A_all = (TH1F*)fin->Get("h_var_A_data");
  TH1F *h_var_B_all = (TH1F*)fin->Get("h_var_B_data");
  TH1F *h_var_C_all = (TH1F*)fin->Get("h_var_C_data");
  TH1F *h_var_D_all = (TH1F*)fin->Get("h_var_D_data");
  TH1F *h_var_A = (TH1F*)fin->Get(("h_var_A_"+name+"_mc").c_str());
  TH1F *h_var_B = (TH1F*)fin->Get(("h_var_B_"+name+"_mc").c_str());
  TH1F *h_var_C = (TH1F*)fin->Get(("h_var_C_"+name+"_mc").c_str());
  TH1F *h_var_D = (TH1F*)fin->Get(("h_var_D_"+name+"_mc").c_str());
  TH1F *h_var_A_sig = (TH1F*)fin->Get(("h_var_A_"+name+"_sig_mc").c_str());
  TH1F *h_var_B_sig = (TH1F*)fin->Get(("h_var_B_"+name+"_sig_mc").c_str());
  TH1F *h_var_C_sig = (TH1F*)fin->Get(("h_var_C_"+name+"_sig_mc").c_str());
  TH1F *h_var_D_sig = (TH1F*)fin->Get(("h_var_D_"+name+"_sig_mc").c_str());
  TH1F *h_var_A_had = (TH1F*)fin->Get(("h_var_A_"+name+"_had_mc").c_str());
  TH1F *h_var_B_had = (TH1F*)fin->Get(("h_var_B_"+name+"_had_mc").c_str());
  TH1F *h_var_C_had = (TH1F*)fin->Get(("h_var_C_"+name+"_had_mc").c_str());
  TH1F *h_var_D_had = (TH1F*)fin->Get(("h_var_D_"+name+"_had_mc").c_str());


  std::cout<<"Reading "<<name<<std::endl;
  const int nFiles = fileList.size();
  int eventnum = 1;
  for (int i = 0; i < nFiles; i++){
    vector<string> subfileList = return_file_list({fileList.at(i)});
    TTreeReader *vbswy_treeReader;
    TTreeReader *DAOD_treeReader;
    TChain vbswy_chain("vbswy");
    TChain DAOD_chain("DAOD_tree");
    for (const auto fName: subfileList){
      TFile *f = new TFile(("root://jmcgowan.cern.ch/"+fName).c_str(),"READ");
      if(!f->IsOpen()){
	throw runtime_error("Unable to open file");
      }
      vbswy_chain.Add(fName.c_str());
      DAOD_chain.Add(fName.c_str());
      f->Close();
      //delete f;
    }

    vbswy_treeReader = new TTreeReader(&vbswy_chain);
    DAOD_treeReader = new TTreeReader(&DAOD_chain);

    TTreeReaderValue<double> nn_score(*vbswy_treeReader, "nn_score");
    TTreeReaderValue<int> gam_n(*vbswy_treeReader, "gam_n");
    TTreeReaderValue<int> j_n(*vbswy_treeReader, "j_n");
    TTreeReaderValue<int> lep_n(*vbswy_treeReader, "lep_n");
    TTreeReaderValue<int> pass_ly_Z_veto(*vbswy_treeReader,"pass_ly_Z_veto");
    TTreeReaderValue<int> pass_second_lepton_veto(*vbswy_treeReader,"pass_second_lepton_veto");
    TTreeReaderValue<double> weight_all(*vbswy_treeReader, "weight_all");
    TTreeReaderValue<double> weight_mc(*vbswy_treeReader, "weight_mc");
    TTreeReaderValue<double> jj_m(*vbswy_treeReader, "jj_m");
    TTreeReaderValue<double> jj_drap(*vbswy_treeReader, "jj_drap");
    TTreeReaderValue<double> w_mt(*vbswy_treeReader, "w_mt");
    TTreeReaderValue<double> met(*vbswy_treeReader, "met");
    TTreeReaderValue<double> j0_pt(*vbswy_treeReader, "j0_pt");
    TTreeReaderValue<double> j1_pt(*vbswy_treeReader, "j1_pt");
    TTreeReaderValue<double> gam_pt(*vbswy_treeReader, "gam_pt");
    TTreeReaderValue<double> gam_eta(*vbswy_treeReader, "gam_eta");
    TTreeReaderValue<double> lep_pt(*vbswy_treeReader, "lep_pt");
    TTreeReaderValue<double> lep_eta(*vbswy_treeReader, "lep_eta");
    TTreeReaderValue<double> e_isoTight(*vbswy_treeReader,"e_isoTight");
    TTreeReaderValue<double> e_idTight(*vbswy_treeReader,"e_idTight");
    TTreeReaderValue<bool> e_passIP(*vbswy_treeReader,"e_passIP");
    TTreeReaderValue<bool> mu_idMedium(*vbswy_treeReader,"mu_idMedium");
    TTreeReaderValue<bool> mu_idTight(*vbswy_treeReader,"mu_idTight");
    TTreeReaderValue<bool> mu_isoFCTight(*vbswy_treeReader,"mu_isoFCTight");
    TTreeReaderValue<bool> mu_passIP(*vbswy_treeReader,"mu_passIP");
    TTreeReaderValue<double> gam_topoETcone40(*vbswy_treeReader, "gam_topoETcone40");
    TTreeReaderValue<double> gam_topoETcone20(*vbswy_treeReader, "gam_topoETcone20");
    TTreeReaderValue<double> gam_pTcone20(*vbswy_treeReader, "gam_pTcone20");
    TTreeReaderValue<bool> gam_isLoosePrime2(*vbswy_treeReader, "gam_isLoosePrime2");
    TTreeReaderValue<bool> gam_isLoosePrime3(*vbswy_treeReader, "gam_isLoosePrime3");
    TTreeReaderValue<bool> gam_idLoose(*vbswy_treeReader, "gam_idLoose");
    TTreeReaderValue<bool> gam_isLoosePrime4a(*vbswy_treeReader, "gam_isLoosePrime4a");
    TTreeReaderValue<bool> gam_isLoosePrime5(*vbswy_treeReader, "gam_isLoosePrime5");
    TTreeReaderValue<bool> gam_idTight(*vbswy_treeReader, "gam_idTight");
    TTreeReaderValue<int> pass_dq(*vbswy_treeReader, "pass_dq");
    TTreeReaderValue<int> pass_jetclean(*vbswy_treeReader, "pass_jetclean");
    TTreeReaderValue<int> pass_onelep(*vbswy_treeReader,"pass_onelep");
    TTreeReaderValue<int> pass_onegam(*vbswy_treeReader,"pass_onegam");
    TTreeReaderValue<int> pass_twojet(*vbswy_treeReader,"pass_twojet");
    TTreeReaderValue<int> pass_trigger(*vbswy_treeReader,"pass_trigger");
    TTreeReaderValue<int> pass_primary_vertex(*vbswy_treeReader, "pass_primary_vertex");
    TTreeReaderValue<int> is_Wenu(*vbswy_treeReader, "is_Wenu");
    TTreeReaderValue<int> is_Wmunu(*vbswy_treeReader, "is_Wmunu");
    TTreeReaderValue<int> gam_truth_type(*vbswy_treeReader,"gam_truth_type");
    TTreeReaderValue<int> gam_truth_origin(*vbswy_treeReader,"gam_truth_origin");
    TTreeReaderValue<bool> pass_vy_OR(*vbswy_treeReader,"pass_vy_OR");
    TTreeReaderValue<double> weight_m(*vbswy_treeReader, "weight_m");
    TTreeReaderValue<double> weight_e(*vbswy_treeReader, "weight_e");
    TTreeReaderValue<int> n_bjets_85(*vbswy_treeReader, "n_bjets_85");
    TTreeReaderValue<int> Wy2jEventType(*vbswy_treeReader, "Wy2jEventType");
    TTreeReaderValue<int> pass_dr(*vbswy_treeReader, "pass_dr");    
    TTreeReaderValue<int> pass_dphi(*vbswy_treeReader, "pass_dphi");
    TTreeReaderValue<int> pass_sr_wy(*vbswy_treeReader, "pass_sr_wy");
    TTreeReaderValue<int> pass_cra_wy(*vbswy_treeReader, "pass_cra_wy");
    TTreeReaderValue<int> pass_crb_wy(*vbswy_treeReader, "pass_crb_wy");
    TTreeReaderValue<int> pass_crc_wy(*vbswy_treeReader, "pass_crc_wy");
    TTreeReaderValue<int> n_gapjets(*vbswy_treeReader, "n_gapjets");
    TTreeReaderValue<double> ly_xi(*vbswy_treeReader, "ly_xi");
    TTreeReaderValue<double> wy_xi(*vbswy_treeReader, "wy_xi");
    TTreeReaderValue<double> jj_dphi_signed(*vbswy_treeReader, "jj_dphi_signed");
    TTreeReaderValue<double> jj_pt(*vbswy_treeReader, "jj_pt");

    //Values from DAOD tree
    TTreeReaderValue<double> luminosity(*DAOD_treeReader, "luminosity");
    TTreeReaderValue<double> xsection(*DAOD_treeReader, "xsection");
    TTreeReaderValue<double> sum_of_weights(*DAOD_treeReader, "sum_of_weights");
    TTreeReaderValue<double> kfactor(*DAOD_treeReader, "kfactor");
    TTreeReaderValue<double> geneff(*DAOD_treeReader, "geneff");
    TTreeReaderValue<double> dsid(*DAOD_treeReader, "dsid");

    DAOD_treeReader->Next();
    double weight;
    double sumofweights = *sum_of_weights;
    double xs = *xsection * (*geneff) * (*kfactor);
    double lumi = *luminosity;
    int dataset = *dsid;
    while(DAOD_treeReader->Next()){
      sumofweights += *sum_of_weights;
    }
    if (isData & !isMCClosure){
      xs = 1.0;
      lumi = 1.0;
      sumofweights = 1.0;
    }
    int counter = 0;
    while (vbswy_treeReader->Next()){
      counter++;
      if (debug && counter == 100){
        break;
      }
      weight = *weight_all * (*weight_mc) *xs * lumi / sumofweights;
      //if ((dataset >= 700015 && dataset <= 700017) || (dataset >= 504678 && dataset <= 504680)){
        weight = .5 * weight; //half weight for QCD samples, to take average of two QCD generators
      //}

      //Make selection
      bool signal_selec = ((dataset > 363272) || (dataset < 363270)) || (*Wy2jEventType > 0 && *Wy2jEventType < 3);
      bool pass_baseline_e = ( (*pass_onegam)&&(*pass_trigger)&&(*pass_twojet)&&(*pass_primary_vertex)&&
                      (*pass_dq)&&(*pass_jetclean)&&(*pass_vy_OR)&&(*is_Wenu)&&(*pass_onelep)&&
                      (*e_passIP)&&(*e_isoTight==1.0)&&(*e_idTight==1.0)&&
                      (*pass_second_lepton_veto)&&(*gam_pt>22.0)&&(*lep_pt > 30.0)&&(*met > 30.0)&&
                      (*lep_eta > -2.5)&&(*lep_eta < 2.5)&&(*gam_eta > -2.37)&&(*gam_eta < 2.37)&&
                      (*w_mt > 30.0)&&(*pass_ly_Z_veto)&&(*pass_dphi)&&(*pass_dr)&&
                      (*j0_pt > 50.0)&&(*j1_pt > 50.0)&&(*n_bjets_85 == 0) );

      bool pass_baseline_mu = ( (*pass_onegam)&&(*pass_trigger)&&(*pass_twojet)&&(*pass_primary_vertex)&&
                       (*pass_dq)&&(*pass_jetclean)&&(*pass_vy_OR)&&(*is_Wmunu)&&(*pass_onelep)&&
                       (*mu_passIP)&&(*mu_isoFCTight==1.0)&&(*mu_idTight==1.0)&&
                       (*pass_second_lepton_veto)&&(*gam_pt>22.0)&&(*lep_pt > 30.0)&&(*met > 30.0)&&
                       (*lep_eta > -2.5)&&(*lep_eta < 2.5)&&(*gam_eta > -2.37)&&(*gam_eta < 2.37)&&
                       (*w_mt > 30.0)&&(*pass_ly_Z_veto)&&(*pass_dphi)&&(*pass_dr)&&
                       (*j0_pt > 50.0)&&(*j1_pt > 50.0)&&(*n_bjets_85 == 0) );
      bool selection;
      if (channel == "combined"){
        selection = (pass_baseline_e || pass_baseline_mu);
      }
      else{
        selection = ( (channel == "electron") ? pass_baseline_e : pass_baseline_mu);
      }
      selection = selection && signal_selec && (*jj_drap > 2) && (*jj_m > 500);

     if (region == "sr"){
        selection = selection && (*n_gapjets==0) && (*wy_xi <= 0.35);
      }
      if (region == "cr1"){
        selection = selection && (*n_gapjets>=1) && (*wy_xi <= 0.35);
      }
      if (region == "cr2"){
        selection = selection && (*n_gapjets>=1) && (*wy_xi > 0.35);
      }
      if (region == "cr3"){
        selection = selection && (*n_gapjets==0) && (*wy_xi <= 0.35);
      }
      selection = selection && (weight > -10);

      truth_photon = (get_truth_type(*gam_truth_type,*gam_truth_origin) == "photon");
      truth_hadron = (get_truth_type(*gam_truth_type,*gam_truth_origin) == "hadron");
      variable = (*nn_score); // Variable to perform estimate in
      selection = selection && (variable > edges[0]); // Make sure variable is at least at minimum bin 
      if (selection){
	iso = ((*gam_topoETcone40)) - .022 * *gam_pt;
	if ((*gam_idTight) && (iso < 2.45)){
	  if (variable > edges[NBINS]){
	    if (truth_photon){
	      h_var_A_sig->AddBinContent(NBINS,weight);
	    }
	    else if (truth_hadron){
	      h_var_A_had->AddBinContent(NBINS,weight);
	    }
	    else if (!isData){
	      h_var_A->AddBinContent(NBINS,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_A_all->AddBinContent(NBINS,weight);
	    }
	  }
	  else{
	    if (truth_photon){
	      h_var_A_sig->Fill(variable,weight);
	    }
	    else if (truth_hadron){
	      h_var_A_had->Fill(variable,weight);
	    }
	    else if (!isData){
	      h_var_A->Fill(variable,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_A_all->Fill(variable,weight);
	    }
	  }
	}
	else if ((*gam_idTight) && (iso > (2.45 + gapValue))){
	  if (variable > edges[NBINS]){
	    if (truth_photon){
	      h_var_B_sig->AddBinContent(NBINS,weight);
	    }
	    else if (truth_hadron){
	      h_var_B_had->AddBinContent(NBINS,weight);
	    }
	    else if (!isData){
	      h_var_B->AddBinContent(NBINS,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_B_all->AddBinContent(NBINS,weight);
	    }
	  }
	  else{
	    if (truth_photon){
	      h_var_B_sig->Fill(variable,weight);
	    }
	    else if (truth_hadron){
	      h_var_B_had->Fill(variable,weight);
	    }
	    else if (!isData){
	      h_var_B->Fill(variable,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_B_all->Fill(variable,weight);
	    }
	  }
	}
	else if ((*gam_idLoose) && (!(*gam_idTight)) && (iso < 2.45)){
	  if (variable > edges[NBINS]){
	    if (truth_photon){
	      h_var_C_sig->AddBinContent(NBINS,weight);
	    }
	    else if (truth_hadron){
	      h_var_C_had->AddBinContent(NBINS,weight);
	    }
	    else if (!isData){
	      h_var_C->AddBinContent(NBINS,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_C_all->AddBinContent(NBINS,weight);
	    }
	  }
	  else{
	    if (truth_photon){
	      h_var_C_sig->Fill(variable,weight);
	    }
	    else if (truth_hadron){
	      h_var_C_had->Fill(variable,weight);
	    }
	    else if (!isData){
	      h_var_C->Fill(variable,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_C_all->Fill(variable,weight);
	    }
	  }
	}
	else if ((*gam_idLoose) && (!(*gam_idTight)) && (iso > (2.45 + gapValue))){
	  if (variable > edges[NBINS]){
	    if (truth_photon){
	      h_var_D_sig->AddBinContent(NBINS,weight);
	    }
	    else if (truth_hadron){
	      h_var_D_had->AddBinContent(NBINS,weight);
	    }
	    else if (!isData){
	      h_var_D->AddBinContent(NBINS,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_D_all->AddBinContent(NBINS,weight);
	    }
	  }
	  else{
	    if (truth_photon){
	      h_var_D_sig->Fill(variable,weight);
	    }
	    else if (truth_hadron){
	      h_var_D_had->Fill(variable,weight);
	    }
	    else if (!isData){
	      h_var_D->Fill(variable,weight);
	    }
	    if (isData || isMCClosure){
	      h_var_D_all->Fill(variable,weight);
	    }
	  }
	} // end of region selection
      } // end of selected event
    }  // end of while loop
  } // end of loop over files


  TFile *fout = new TFile(output_file.c_str(), "Update");
  fin->cd();
  h_var_A_all->Write(0, TObject::kWriteDelete);
  h_var_B_all->Write(0, TObject::kWriteDelete);
  h_var_C_all->Write(0, TObject::kWriteDelete);
  h_var_D_all->Write(0, TObject::kWriteDelete);
  h_var_A->Write(0, TObject::kWriteDelete);
  h_var_B->Write(0, TObject::kWriteDelete);
  h_var_C->Write(0, TObject::kWriteDelete);
  h_var_D->Write(0, TObject::kWriteDelete);
  h_var_A_sig->Write(0, TObject::kWriteDelete);
  h_var_B_sig->Write(0, TObject::kWriteDelete);
  h_var_C_sig->Write(0, TObject::kWriteDelete);
  h_var_D_sig->Write(0, TObject::kWriteDelete);
  h_var_A_had->Write(0, TObject::kWriteDelete);
  h_var_B_had->Write(0, TObject::kWriteDelete);
  h_var_C_had->Write(0, TObject::kWriteDelete);
  h_var_D_had->Write(0, TObject::kWriteDelete);
  fin->Close();
  return;
}


void initialize_histograms(vector<string> samples)
{

  TFile *fin = new TFile(output_file.c_str(), "Update");
  TH1F *h_n_bkg = new TH1F("h_n_bkg",(";"+label+";N^{bkg}_{A}").c_str(),NBINS,edges);
  TH1F *h_purity = new TH1F("h_purity",(";"+label+";Photon purity").c_str(),NBINS,edges);
  TH1F *h_cA = new TH1F("h_cA",(";"+label+";c_{B}").c_str(),NBINS,edges);
  TH1F *h_cB = new TH1F("h_cB",(";"+label+";c_{B}").c_str(),NBINS,edges);
  TH1F *h_cC = new TH1F("h_cC",(";"+label+";c_{C}").c_str(),NBINS,edges);
  TH1F *h_cD = new TH1F("h_cD",(";"+label+";c_{D}").c_str(),NBINS,edges);
  TH1F *h_R = new TH1F("h_R",(";"+label+";R").c_str(),NBINS,edges);


  string title = ";"+label+";events";
  TH1F *h_var_A_all = new TH1F("h_var_A_data",title.c_str(), NBINS, edges);
  TH1F *h_var_B_all = new TH1F("h_var_B_data",title.c_str(), NBINS, edges);
  TH1F *h_var_C_all = new TH1F("h_var_C_data",title.c_str(), NBINS, edges);
  TH1F *h_var_D_all = new TH1F("h_var_D_data",title.c_str(), NBINS, edges);

  h_var_A_all->Sumw2();
  h_var_B_all->Sumw2();
  h_var_C_all->Sumw2();
  h_var_D_all->Sumw2();

  h_n_bkg->Write(0, TObject::kWriteDelete);
  h_cA->Write(0,TObject::kWriteDelete);
  h_cB->Write(0, TObject::kWriteDelete);
  h_cC->Write(0, TObject::kWriteDelete);
  h_cD->Write(0, TObject::kWriteDelete);
  h_R->Write(0, TObject::kWriteDelete);
  h_var_A_all->Write(0, TObject::kWriteDelete);
  h_var_B_all->Write(0, TObject::kWriteDelete);
  h_var_C_all->Write(0, TObject::kWriteDelete);
  h_var_D_all->Write(0, TObject::kWriteDelete);
  
  for (int i = 0; i < samples.size(); i++){
    string name = samples.at(i);
    TH1F *h_var_A = new TH1F(("h_var_A_"+name+"_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_B = new TH1F(("h_var_B_"+name+"_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_C = new TH1F(("h_var_C_"+name+"_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_D = new TH1F(("h_var_D_"+name+"_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_A_sig = new TH1F(("h_var_A_"+name+"_sig_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_B_sig = new TH1F(("h_var_B_"+name+"_sig_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_C_sig = new TH1F(("h_var_C_"+name+"_sig_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_D_sig = new TH1F(("h_var_D_"+name+"_sig_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_A_had = new TH1F(("h_var_A_"+name+"_had_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_B_had = new TH1F(("h_var_B_"+name+"_had_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_C_had = new TH1F(("h_var_C_"+name+"_had_mc").c_str(),title.c_str(), NBINS, edges);
    TH1F *h_var_D_had = new TH1F(("h_var_D_"+name+"_had_mc").c_str(),title.c_str(), NBINS, edges);

    h_var_A->Sumw2();
    h_var_B->Sumw2();
    h_var_C->Sumw2();
    h_var_D->Sumw2();
    h_var_A_sig->Sumw2();
    h_var_B_sig->Sumw2();
    h_var_C_sig->Sumw2();
    h_var_D_sig->Sumw2();
    h_var_A_had->Sumw2();
    h_var_B_had->Sumw2();
    h_var_C_had->Sumw2();
    h_var_D_had->Sumw2();

    h_var_A->Write(0, TObject::kWriteDelete);
    h_var_B->Write(0, TObject::kWriteDelete);
    h_var_C->Write(0, TObject::kWriteDelete);
    h_var_D->Write(0, TObject::kWriteDelete);
    h_var_A_sig->Write(0, TObject::kWriteDelete);
    h_var_B_sig->Write(0, TObject::kWriteDelete);
    h_var_C_sig->Write(0, TObject::kWriteDelete);
    h_var_D_sig->Write(0, TObject::kWriteDelete);
    h_var_A_had->Write(0, TObject::kWriteDelete);
    h_var_B_had->Write(0, TObject::kWriteDelete);
    h_var_C_had->Write(0, TObject::kWriteDelete);
    h_var_D_had->Write(0, TObject::kWriteDelete);

    delete h_var_A;
    delete h_var_B;
    delete h_var_C;
    delete h_var_D;
    delete h_var_A_sig;
    delete h_var_B_sig;
    delete h_var_C_sig;
    delete h_var_D_sig;
    delete h_var_A_had;
    delete h_var_B_had;
    delete h_var_C_had;
    delete h_var_D_had;
    
  }

  fin->Close();

}

void read_config(const char *inputConfig)
{
  TEnv config;

  config.ReadFile(inputConfig, EEnvLevel(0));

  outfile = config.GetValue("directory","None");

  channel = config.GetValue("channel","combined");

  lp_def = config.GetValue("Loose_def",4);
  region = config.GetValue("Region","None");

  mc_closure = config.GetValue("MC_Closure",0);

  debug = config.GetValue("Debug",0);

  config.Print();
}


int main(int argc, char **argv)
{

  if (argc < 2){
    cout<<"Invalid call to MakeTemplateDataExe"<<endl;
    cout<<"Valid call:"<<endl;
    cout<<"./MakeABCDDataExe configfile.config"<<endl;
    return 1;
  }

  const char *inputConfig = argv[1];
  read_config(inputConfig);

  if (mc_closure){
    output_file = outfile+"/ABCD_MCClosure.root";
  }
  else{
    output_file = outfile+"/ABCD_Data.root";
  }

  vector<string> samples = {"signal","qcd","wjets","zjets","ttbar","singletop","tWgamma","ttgamma","zgamma","ewkzgamma","WVBF","ZVBF"};
  initialize_histograms(samples);
  if (mc_closure){
    readTrees(fileList_signal_mc, 0,1, "signal");
    readTrees(fileList_qcd_mc, 0,1, "qcd");
    readTrees(fileList_wjets_mc, 0,1, "wjets");
    readTrees(fileList_zjets_mc, 0,1, "zjets");
    //readTrees(fileList_photonjets_mc, 0,1, "photonjets");
    readTrees(fileList_ttbar_mc, 0,1, "ttbar");
    readTrees(fileList_singletop_mc, 0,1, "singletop");
    readTrees(fileList_tWgamma_mc, 0,1, "tWgamma");
    readTrees(fileList_ttgamma_mc, 0,1, "ttgamma");
    readTrees(fileList_zgamma_mc, 0,1, "zgamma");
    readTrees(fileList_ewkzgamma_mc,0,1,"ewkzgamma");
    readTrees(fileList_WVBF_mc, 0,1, "WVBF");
    readTrees(fileList_ZVBF_mc, 0,1, "ZVBF");
  }
  else{
    readTrees(fileList_data, 1,0, "data");
    readTrees(fileList_signal_mc, 0,0, "signal");
    readTrees(fileList_qcd_mc, 0,0, "qcd");
    readTrees(fileList_wjets_mc, 0,0, "wjets");
    readTrees(fileList_zjets_mc, 0,0, "zjets");
    //readTrees(fileList_photonjets_mc, 0,0, "photonjets");
    readTrees(fileList_ttbar_mc, 0,0, "ttbar");
    readTrees(fileList_singletop_mc, 0,0, "singletop");
    readTrees(fileList_tWgamma_mc, 0,0, "tWgamma");
    readTrees(fileList_ttgamma_mc, 0,0, "ttgamma");
    readTrees(fileList_zgamma_mc, 0,0, "zgamma");
    readTrees(fileList_ewkzgamma_mc,0,0,"ewkzgamma");
    readTrees(fileList_WVBF_mc, 0,0, "WVBF");
    readTrees(fileList_ZVBF_mc, 0,0, "ZVBF");
  }
}


