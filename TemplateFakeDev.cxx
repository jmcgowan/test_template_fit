// C++ and ROOT includeds
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <cstring>
#include <map>
#include <stdio.h>
#include<cmath>
#include <typeinfo>
#include <sys/resource.h>
#include "/afs/cern.ch/user/j/jmcgowan/tdrstyle.C"

#include "TFile.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TH1.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TChain.h"
#include "TLine.h"
#include<TRandom.h>
#include "TLorentzVector.h"
#include "TEnv.h"

// RooFit includes
#include "RooGaussExp.h"
#include "RooGaussDoubleSidedExp.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooMultiVarGaussian.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooChebychev.h"
#include "RooFitResult.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "RooFit.h"
#include "RooWorkspace.h"
#include "RooProdPdf.h"
#include "RooCBShape.h"
#include "RooBukinPdf.h"
#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooExponential.h"
#include "RooNovosibirsk.h"
#include "RooLandau.h"
#include "RooGExpModel.h"
#include "RooJohnson.h"
#include "RooKeysPdf.h"
#include "RooCrystalBall.h"

// watch memory
#include <sys/resource.h>
int who = RUSAGE_SELF;
struct rusage usage;

using namespace std;
using namespace RooFit;

#include "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/utils.C"



// GLOBAL VARIABLES

//channel - choose "electron", "muon" or "combined"
string channel;// "combined";

//Normalization
string normalization;// "13 TeV 139 fb^{-1}";

string fit_range;// "-20 < x && x < 40";
double range_low;// -20;
double range_high;// 40;

//Leak fit - "ExpGaus" or "Double_CBall"
string LeakFit;// "ExpGaus";
//Tight fit - "ExpGaus" or "Double_CBall"
string TightFit;// "Double_CBall";
//Loose fit - "Novosibirsk" or "Bukin";
string LooseFit;// "Novosibirsk";

//Propagate stat uncertainty on fits
bool propagate_fit_errors;// 1;

//is MC closure test
bool mc_closure;// 1;

//Global variables
string outfile;
string histofile;
string outputpdf;
string output_txt;
string in_workspace;
string out_workspace;
string errtxt;
string notes;
string notes2;

//ANALYSIS BINS
string label;// "NN score";
const Int_t NBINS = 5;
Double_t edges[NBINS+1] = {0,.2,.4,.6,.8,1.0};
//string label = "pT_{jj}";
//const Int_t NBINS = 6;
//Double_t edges[NBINS+1] = {100,140,200,275,400,550,1050};

Double_t minimums[NBINS];

RooDataSet *w_gam_temp[NBINS], *w_jet_temp[NBINS], *w_signal[NBINS], *w_leakage[NBINS], *w_tight_signal[NBINS], *w_loose_signal[NBINS], *w_nt_jet_mc[NBINS];
RooDataSet *w_leakage_combined;
RooRealVar *w, *var_iso;
RooWorkspace *wkspace;

struct valerrors
{
  vector<double> values;
  vector<double> errors;
};

struct valerror
{
  double value;
  double error;
};

string to_trunc_string(float num)
{
   std::stringstream stream;
   stream << std::fixed << std::setprecision(2) << num;
   string s = stream.str();
   return s;
}

struct FitInfo
{
  RooAbsPdf *shape;  
  RooFitResult *r;
};

FitInfo fit_2Scrystalball(RooDataSet* set, string type, string bin)
{
  FitInfo f;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR) ;
  string range;
  if (type =="Tight"){
    var_iso->setRange("iso",range_low,range_high);
    range = fit_range;
  }
  else if (type =="Leak"){
    var_iso->setRange("iso",range_low,range_high);
    range = fit_range;
  }
  else{
    var_iso->setRange("iso",range_low,range_high);
    range = fit_range;
  }

  RooRealVar *mean = new RooRealVar(("cb2_mean_"+type+bin).c_str(),"cb2_mean",-1,-5,1);
  RooRealVar *sigma = new RooRealVar(("cb2_sigma_"+type+bin).c_str(),"cb2_sigma",2,0,4);
  RooRealVar *alpha = new RooRealVar(("cb2_alpha_"+type+bin).c_str(),"cb2_alpha",-1.0,-10.0,0.0);
  RooRealVar *n = new RooRealVar(("cb2_n_"+type+bin).c_str(),"cb2_n",.1,0,5);
  RooRealVar *alpha2 = new RooRealVar(("cb2_alpha2_"+type+bin).c_str(),"cb2_alpha2",1.0,0.0,10.0);
  RooRealVar *n2 = new RooRealVar(("cb2_n2_"+type+bin).c_str(),"cb2_n2",.1,0,5);
  RooCrystalBall *Double_CBall = new RooCrystalBall(("Double_CBall_"+type+bin).c_str(), "Double_CBall",*var_iso,*mean,*sigma,*alpha,*n,*alpha2,*n2);

  cout<<"----Fitting "<<type<<" 2 sided crystalball----"<<endl; 
  f.r = Double_CBall->fitTo(*set,Range("iso"),SumW2Error(kTRUE),Save()/*,PrintEvalErrors(-1)*/,Verbose(kFALSE)/*,PrintLevel(-1)*/); 
  f.r->correlationMatrix(); //Throws error if corr matrix is singular
  cout<<"-----Fit "<<type<<" Template-----"<<endl;

  f.shape = Double_CBall;

  //Now that the fit is done, make gaussian constraints to be used by subsequent fits
  RooMultiVarGaussian *gaus_constraint = new RooMultiVarGaussian(("Double_CBall_"+type+"_gaus_constraints"+bin).c_str(),("Double_CBall_"+type+"_gaus_constraints"+bin).c_str(),*(f.shape->getVariables()),*f.r,kFALSE);

  wkspace->import(*Double_CBall);
  wkspace->import(*gaus_constraint);
  return f;
}

//ExpGaus implementation
FitInfo fit_expgaus(RooDataSet* set, string type, string bin)
{
  FitInfo f;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR) ;
  string range;
  if (type =="Tight"){
    var_iso->setRange("iso",range_low,range_high);
    range = fit_range;
  }
  else if (type =="Leak"){
    var_iso->setRange("iso",range_low,range_high);
    range = fit_range;
  }
  else{
    var_iso->setRange("iso",range_low,range_high);
    range = fit_range;
  }
  RooRealVar *mean = new RooRealVar(("ExpGaus_mean_"+type+bin).c_str(),"ExpGaus_mean",-1,-5,1);
  RooRealVar *sigma = new RooRealVar(("ExpGaus_sigma_"+type+bin).c_str(),"ExpGaus_sigma",1,0.1,4);
  RooAbsReal *alpha = new RooRealVar(("ExpGaus_alpha_"+type+bin).c_str(),"ExpGaus_alpha",1.0);
  RooRealVar *alpha2 = new RooRealVar(("ExpGaus_alpha2_"+type+bin).c_str(),"ExpGaus_alpha2",0.5,-1.0,3.0);
  RooGaussDoubleSidedExp *ExpGaus = new RooGaussDoubleSidedExp(("ExpGaus_"+type+bin).c_str(), "ExpGaus",*var_iso,*mean,*sigma,*alpha,*alpha2);
 
  cout<<"----Fitting "<<type<<" ExpGaus----"<<endl; 
  f.r = ExpGaus->fitTo(*set,Range("iso"),SumW2Error(kTRUE),Save()/*,PrintEvalErrors(-1)*/,Verbose(kFALSE)/*,PrintLevel(-1)*/); 
  f.r->correlationMatrix(); //Throws error if corr matrix is singular
  cout<<"-----Fit "<<type<<" Template-----"<<endl;

  f.shape = ExpGaus;

  //Now that the fit is done, make gaussian constraints to be used by subsequent fits
  RooMultiVarGaussian *gaus_constraint = new RooMultiVarGaussian(("ExpGaus_"+type+"_gaus_constraints"+bin).c_str(),("ExpGaus_"+type+"_gaus_constraints"+bin).c_str(),*(f.shape->getVariables()),*f.r,kFALSE);

  wkspace->import(*ExpGaus);
  wkspace->import(*gaus_constraint);
  return f;
}


FitInfo fit_loose_novosibirsk(string bin, string leak_fit)
{

  FitInfo f;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR) ;
  string range;
  RooDataSet *set = w_jet_temp[stoi(bin)];
  var_iso->setRange("iso",range_low,range_high);
  range = fit_range;
  string type = "Non-tight";

  RooRealVar *peak = new RooRealVar(("Novosibirsk_peak_"+type+bin).c_str(),"Novosibirsk_peak",0,-2,3);
  RooRealVar *width = new RooRealVar(("Novosibirsk_width_"+type+bin).c_str(),"Novosibirsk_width",6,0.2,8);
  RooRealVar *tail = new RooRealVar(("Novosibirsk_tail_"+type+bin).c_str(),"Novosibirsk_tail",0,-5,5);
  RooNovosibirsk *novo = new RooNovosibirsk(("Novosibirsk_"+type+bin).c_str(),"Novosibirsk",*var_iso,*peak,*width,*tail);

  // Add leakage shape
  RooAbsPdf *leak_shape = wkspace->pdf((leak_fit+"_Leak0").c_str()); 

  RooRealVar *n1 = new RooRealVar(("nleak_novo1_"+bin).c_str(),"#bkg events",w_jet_temp[stoi(bin)]->sumEntries(range.c_str())-w_leakage[stoi(bin)]->sumEntries(range.c_str()),0,(w_jet_temp[stoi(bin)]->sumEntries(range.c_str())-w_leakage[stoi(bin)]->sumEntries(range.c_str()))*2.0);
  RooRealVar *n2 = new RooRealVar(("nleak_novo2_"+bin).c_str(),"#leakage events",w_leakage[stoi(bin)]->sumEntries(range.c_str()),w_leakage[stoi(bin)]->sumEntries(range.c_str())-0.1,w_leakage[stoi(bin)]->sumEntries(range.c_str())+0.1);
  RooAddPdf *model = new RooAddPdf(("Novosibirsk_"+type+"Leak"+bin).c_str(),"Novosibirsk",RooArgList(*novo,*leak_shape),RooArgList(*n1,*n2));

  //Get gaussian constraint from leakage
  RooMultiVarGaussian *leak_constraint = (RooMultiVarGaussian*)wkspace->pdf((leak_fit+"_Leak_gaus_constraints0").c_str());

  cout<<"---------Fitting Loose Novosibirsk----------"<<endl;
  f.r = model->fitTo(*set,Range("iso"),Save(),SumW2Error(kTRUE),ExternalConstraints(*leak_constraint)/*,PrintEvalErrors(-1)*/,Verbose(kFALSE)/*,PrintLevel(-1)*/);
  f.r->correlationMatrix(); //Throws error if corr matrix is singular
  cout<<"----------Fit Loose Novosibirsk-------------------"<<endl;
  f.shape = model;

  //Now that the fit is done, make gaussian constraints to be used by subsequent fits
  //Note we make a constraint only of the terms parameterizing the jet faking photon, i.e. the novosibirsk, and not the terms parameterizing the leakage
  RooMultiVarGaussian *gaus_constraint = new RooMultiVarGaussian(("Novosibirsk_Non-tight_gaus_constraints"+bin).c_str(),("Novosibirsk_Non-tight_gaus_constraints"+bin).c_str(),*(novo->getVariables()),*f.r,kFALSE);

  wkspace->import(*model);
  wkspace->import(*novo);
  wkspace->import(*gaus_constraint);
  return f;

}

FitInfo fit_loose_bukin(string bin, string leak_fit)
{

  FitInfo f;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR) ;
  string range;
  RooDataSet *set = w_jet_temp[stoi(bin)];
  var_iso->setRange("iso",range_low,range_high);
  range = fit_range;
  string type = "Non-tight";

  RooRealVar *peak = new RooRealVar(("Bukin_peak_"+type+bin).c_str(),"Bukin_peak",0,-2,3);
  RooRealVar *width = new RooRealVar(("Bukin_width_"+type+bin).c_str(),"Bukin_width",6,0.2,8);
  RooRealVar *tail = new RooRealVar(("Bukin_tail_"+type+bin).c_str(),"Bukin_tail",0,-5,5);
  RooRealVar *rho1 = new RooRealVar(("b_rho1_"+type+bin).c_str(),"Bukin_rho1",0,-4,5);
  RooRealVar *rho2 = new RooRealVar(("b_rho2_"+type+bin).c_str(),"Bukin_rho2",0,-4,5);
  RooBukinPdf *bukin = new RooBukinPdf(("Bukin_"+type+bin).c_str(),"Bukin",*var_iso,*peak,*width,*tail,*rho1,*rho2);

  // Add leakage shape
  RooAbsPdf *leak_shape = wkspace->pdf((leak_fit+"_Leak0").c_str()); 

  RooRealVar *n1 = new RooRealVar(("nleak_bukin1_"+bin).c_str(),"#bkg events",w_jet_temp[stoi(bin)]->sumEntries(range.c_str())-w_leakage[stoi(bin)]->sumEntries(range.c_str()),0,(w_jet_temp[stoi(bin)]->sumEntries(range.c_str())-w_leakage[stoi(bin)]->sumEntries(range.c_str()))*2.0);
  RooRealVar *n2 = new RooRealVar(("nleak_bukin2_"+bin).c_str(),"#leakage events",w_leakage[stoi(bin)]->sumEntries(range.c_str()),w_leakage[stoi(bin)]->sumEntries(range.c_str())-0.1,w_leakage[stoi(bin)]->sumEntries(range.c_str())+0.1);
  RooAddPdf *model = new RooAddPdf(("Bukin_"+type+"Leak"+bin).c_str(),"Bukin",RooArgList(*bukin,*leak_shape),RooArgList(*n1,*n2));

  //Get gaussian constraint from leakage
  RooMultiVarGaussian *leak_constraint = (RooMultiVarGaussian*)wkspace->pdf((leak_fit+"_Leak_gaus_constraints0").c_str());

  cout<<"---------Fitting Loose Bukin----------"<<endl;
  f.r = model->fitTo(*set,Range("iso"),Save(),SumW2Error(kTRUE),ExternalConstraints(*leak_constraint)/*,PrintEvalErrors(-1)*/,Verbose(kFALSE)/*,PrintLevel(-1)*/);
  f.r->correlationMatrix(); //Throws error if corr matrix is singular
  cout<<"----------Fit Loose Bukin-------------------"<<endl;
  f.shape = model;

  //Now that the fit is done, make gaussian constraints to be used by subsequent fits
  //Note we make a constraint only of the terms parameterizing the jet faking photon, i.e. the bukin, and not the terms parameterizing the leakage
  RooMultiVarGaussian *gaus_constraint = new RooMultiVarGaussian(("Bukin_Non-tight_gaus_constraints"+bin).c_str(),("Bukin_Non-tight_gaus_constraints"+bin).c_str(),*(bukin->getVariables()),*f.r,kFALSE);

  wkspace->import(*model);
  wkspace->import(*bukin);
  wkspace->import(*gaus_constraint);
  return f;

}

double do_loose_fit(int bin, string loose_fit, string leak_fit, TCanvas *canvas)
{

  RooDataSet *set;
  set = w_jet_temp[bin];
  string gam_type;
  gam_type = "Fake #gamma";
  RooPlot* frame = var_iso->frame();
  FitInfo f;  
  if (loose_fit == "Novosibirsk"){
    f = fit_loose_novosibirsk(to_string(bin),leak_fit);
  }
  if (loose_fit == "Bukin"){
    f = fit_loose_bukin(to_string(bin),leak_fit);
  }

  w_leakage[bin]->plotOn(frame,MarkerColor(kRed),Name("leakage"));
  if (mc_closure){
    w_nt_jet_mc[bin]->plotOn(frame,MarkerColor(kBlue),Name("jets"));
  }
  set->plotOn(frame,Name("data"));
  f.shape->plotOn(frame,LineColor(kGreen),Name("Combined shape"));
  f.shape->plotOn(frame,LineColor(kGreen),Name("Combined shape"),RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  f.shape->plotOn(frame,Components((loose_fit+"_Non-tight"+to_string(bin)).c_str()),LineColor(kBlue),Name("jet_shape"));
  f.shape->plotOn(frame,Components((loose_fit+"_Non-tight"+to_string(bin)).c_str()),LineColor(kBlue),Name("jet_shape"),RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  f.shape->plotOn(frame,Components((leak_fit+"_Leak0").c_str()),LineColor(kRed),Name("leak_shape"));
  f.shape->plotOn(frame,Components((leak_fit+"_Leak0").c_str()),LineColor(kRed),Name("leak_shape"),RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  frame->Draw();
  TLegend *leg1 = new TLegend(0.65,.65,0.9,0.9);
  leg1->SetBorderSize(0);
  leg1->SetFillColor(kWhite);
  leg1->SetLineColor(kWhite);
  leg1->AddEntry("data","Leakage+Non-tight Jets", "P");
  leg1->AddEntry("leakage","Signal Leakage","P");
  leg1->AddEntry("jets","Jets from MC", "P");
  leg1->AddEntry("Combined shape","Combined shape", "L");
  leg1->AddEntry("leak_shape","Signal Leakage Shape", "L");
  leg1->AddEntry("jet_shape","Non-tight Jet Shape", "L");
  leg1->Draw();
  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.025);
  DataText->SetNDC(kTRUE);
  string text = to_trunc_string(edges[bin])+" < "+label+" < "+to_trunc_string(edges[bin+1]);
  string chi2 = "Combined shape #chi^{2} = "+to_string(frame->chiSquare());
  DataText->DrawLatex(0.65,0.55,text.c_str());
  DataText->DrawLatex(0.65,0.45,chi2.c_str());

  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());

  canvas->Print(outputpdf.c_str());
  delete DataText;
  return frame->chiSquare();

}

double do_tight_fit(int bin, string fit, TCanvas *canvas)
{

  RooDataSet *set;
  set = w_gam_temp[bin];
  string gam_type;
  gam_type = "Real #gamma";
  RooPlot* frame = var_iso->frame();
  FitInfo f;
  if (fit == "ExpGaus"){  
    f = fit_expgaus(set, "Tight", to_string(bin));
  }
  else if (fit == "Double_CBall"){  
    f = fit_2Scrystalball(set, "Tight", to_string(bin));
  }

  set->plotOn(frame);
  f.shape->plotOn(frame,RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  f.shape->plotOn(frame);
  frame->Draw();
  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.025);
  DataText->SetNDC(kTRUE);
  string text = to_trunc_string(edges[bin])+" < "+label+" < "+to_trunc_string(edges[bin+1]);
  string chi2 = fit+" fit #chi^{2} = "+to_string(frame->chiSquare());
  DataText->DrawLatex(0.65,0.65,(gam_type+" Template").c_str());
  DataText->DrawLatex(0.65,0.55,text.c_str());
  DataText->DrawLatex(0.65,0.45,chi2.c_str());

  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());

  canvas->Print(outputpdf.c_str());
  delete DataText;
  return frame->chiSquare();


}

valerror do_combined_fit(int bin, string tight_fit, string bkg_fit, TCanvas *canvas)
{
  var_iso->setRange("iso",range_low,range_high);
  string range = fit_range;
  FitInfo f;

  RooAbsPdf *sig_shape = wkspace->pdf((tight_fit+"_Tight"+to_string(bin)).c_str());
  RooAbsPdf *temp_shape = wkspace->pdf((bkg_fit+"_Non-tight"+to_string(bin)).c_str());
  RooRealVar *nsig = new RooRealVar(("nsig_"+to_string(bin)).c_str(),"#signal events",w_gam_temp[bin]->sumEntries(),0,w_gam_temp[bin]->sumEntries()*2);
  RooRealVar *nbkg = new RooRealVar(("nbkg_"+to_string(bin)).c_str(),"#bkg events",w_signal[bin]->sumEntries()-w_gam_temp[bin]->sumEntries(),0,(w_signal[bin]->sumEntries()-w_gam_temp[bin]->sumEntries())*2);
  RooAddPdf *model;
  model = new RooAddPdf(("model"+to_string(bin)).c_str(),"sig+temp",RooArgList(*sig_shape,*temp_shape),RooArgList(*nsig,*nbkg));
 
  //Get gaussian constraints from tight and non-tight fit
  RooMultiVarGaussian *tight_constraint = (RooMultiVarGaussian*)wkspace->pdf((tight_fit+"_Tight_gaus_constraints"+to_string(bin)).c_str());
  RooMultiVarGaussian *loose_constraint = (RooMultiVarGaussian*)wkspace->pdf((bkg_fit+"_Non-tight_gaus_constraints"+to_string(bin)).c_str());
 
  cout<<"----Fitting Signal Data----"<<endl;
  f.r = model->fitTo(*w_signal[bin],Range("iso"),SumW2Error(kTRUE),Save()/*,PrintEvalErrors(-1)*/,Verbose(kFALSE)/*,PrintLevel(-1)*/,RooFit::ExternalConstraints(RooArgSet(*tight_constraint, *loose_constraint)));
  f.r->correlationMatrix();  
  cout<<"----Fit Signal Data------"<<endl;
  //nsig->setConstant(true);
  //nbkg->setConstant(true);
  wkspace->import(*model);
  wkspace->import(*sig_shape);
  wkspace->import(*temp_shape);

  //Plot shapes
  RooPlot* frame = var_iso->frame();
  w_signal[bin]->plotOn(frame,Name("all"));
  if (mc_closure){
    w_loose_signal[bin]->plotOn(frame,MarkerColor(kRed),Name("jets"));
    w_tight_signal[bin]->plotOn(frame,MarkerColor(kBlue),Name("photons"));
  }
  model->plotOn(frame,LineColor(kGreen),Name("combined shape"));
  model->plotOn(frame,LineColor(kGreen),Name("combined shape"),RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  model->plotOn(frame,Components((bkg_fit+"_Non-tight"+to_string(bin)).c_str()),LineColor(kBlue),Name("jet_shape"));
  model->plotOn(frame,Components((bkg_fit+"_Non-tight"+to_string(bin)).c_str()),LineColor(kBlue),Name("jet_shape"),RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  model->plotOn(frame,Components((tight_fit+"_Tight"+to_string(bin)).c_str()),LineColor(kRed),Name("photon_shape"));
  model->plotOn(frame,Components((tight_fit+"_Tight"+to_string(bin)).c_str()),LineColor(kRed),Name("photon_shape"),RooFit::VisualizeError(*f.r,1,false),RooFit::FillColor(TColor::GetColorTransparent(kOrange,0.5)));
  frame->Draw();
  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.025);
  DataText->SetNDC(kTRUE);
  string text = to_trunc_string(edges[bin])+" < "+label+" < "+to_trunc_string(edges[bin+1]);
  DataText->DrawLatex(0.65,0.55,"SR Fit");
  DataText->DrawLatex(0.65,0.45,text.c_str());
  TLegend *leg1 = new TLegend(0.65,.65,0.9,0.9);
  leg1->SetBorderSize(0);
  leg1->SetFillColor(kWhite);
  leg1->SetLineColor(kWhite);
  leg1->AddEntry("all","Tight Data", "P");
  if (mc_closure){
    leg1->AddEntry("photons","Tight Photons from MC","P");
    leg1->AddEntry("jets","j -> #gamma from MC", "P");
  }
  leg1->AddEntry("combined shape","Combined shape", "L");
  leg1->AddEntry("jet_shape","Jet Shape", "L");
  leg1->AddEntry("photon_shape","Real Photon Shape", "L");
  leg1->Draw();
  
  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  //string data_info = "#scale[0.8]{#font[12]{Validation}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  } 
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  } 
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  } 
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}"; 
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());
  
  canvas->Print(outputpdf.c_str());

  //Calculate yield
  valerror yield;
  var_iso->setRange("int_range",-20,2.45);
  RooAbsReal *integral_loose = temp_shape->createIntegral(*var_iso,*var_iso,"int_range");
  double int_loose = integral_loose->getVal();
  double int_err;
  if (propagate_fit_errors){
    int_err = integral_loose->getPropagatedError(*f.r, *var_iso);
  } else{
    int_err = 0;
  }

  yield.value = nbkg->getValV() * int_loose;
  yield.error = TMath::Sqrt( (TMath::Power(nbkg->getValV(),2)*TMath::Power(int_err,2)) + (TMath::Power(int_loose,2)*TMath::Power(nbkg->getError(),2)));

  cout<<"Norm value signal: "<<nsig->getValV()<<" +/- "<<nsig->getError()<<std::endl;
  cout<<"Norm value bkg: "<<nbkg->getValV()<<" +/- "<<nbkg->getError()<<std::endl;
  cout<<"Integral value: "<<int_loose<<" +/- "<<int_err<<std::endl;
  cout<<"Yield: "<<yield.value<<" +/- "<<yield.error<<endl;

  return yield;
}



void plot_dataset(int bin, TCanvas *canvas)
{
  TLatex* DataText = new TLatex;

  RooPlot* frame = var_iso->frame(Bins(40));
  w_gam_temp[bin]->plotOn(frame,MarkerColor(kRed),Name("real_gam_temp"));
  w_jet_temp[bin]->plotOn(frame,MarkerColor(kBlue),Name("fake_gam_temp"));
  w_signal[bin]->plotOn(frame,MarkerColor(kGreen),Name("Tight Data"));
  w_leakage[bin]->plotOn(frame,MarkerColor(kYellow),Name("Leakage"));
  if (mc_closure){
    w_loose_signal[bin]->plotOn(frame,MarkerColor(6),Name("j_fake_y"));
  }
  frame->Draw();
  TLegend* leg = new TLegend(0.65,0.65,.9,.9,NULL);
  leg->AddEntry("Tight Data", "Tight Data","lp");
  leg->AddEntry("real_gam_temp", "Real photon template","lp");
  leg->AddEntry("fake_gam_temp", "Fake photon template","lp");
  leg->AddEntry("Leakage", "Leakage Template","lp");
  if (mc_closure){
    leg->AddEntry("j_fake_y","j -> #gamma from MC","lp");
  }
  leg->SetBorderSize(0);
  leg->Draw();
  DataText->SetTextSize(0.05);
  DataText->SetNDC(kTRUE);
  string text = to_trunc_string(edges[bin])+" < "+label+" < "+to_trunc_string(edges[bin+1]);
  DataText->DrawLatex(0.55,0.45,text.c_str());


  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  //string data_info = "#scale[0.8]{#font[12]{Validation}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());

  canvas->Print(outputpdf.c_str());
  canvas->Clear();
}

void read_in_ws()
{

  wkspace = new RooWorkspace("wkspace");
  TFile *f = new TFile(in_workspace.c_str(),"read");
  RooWorkspace *ws = (RooWorkspace*)f->Get("ws");
  var_iso = ws->var("var_iso");
  w = ws->var("w");

  double min, max;
  for (int i = 0; i < NBINS; i++){
    w_gam_temp[i] = ((RooDataSet*)ws->data(("gam_temp"+to_string(i)).c_str())->Clone(("gam_temp"+to_string(i)).c_str()));
    w_jet_temp[i] = ((RooDataSet*)ws->data(("jet_temp"+to_string(i)).c_str())->Clone(("jet_temp"+to_string(i)).c_str()));
    w_signal[i] = ((RooDataSet*)ws->data(("signal"+to_string(i)).c_str())->Clone(("signal"+to_string(i)).c_str()));
    w_leakage[i] = ((RooDataSet*)ws->data(("leakage"+to_string(i)).c_str())->Clone(("leakage"+to_string(i)).c_str()));
    if (mc_closure){
      w_tight_signal[i] = ((RooDataSet*)ws->data(("tight_signal"+to_string(i)).c_str())->Clone(("tight_signal"+to_string(i)).c_str()));
      w_loose_signal[i] = ((RooDataSet*)ws->data(("loose_signal"+to_string(i)).c_str())->Clone(("loose_signal"+to_string(i)).c_str()));
      w_nt_jet_mc[i] = ((RooDataSet*)ws->data(("nt_jet_mc"+to_string(i)).c_str())->Clone(("nt_jet_mc"+to_string(i)).c_str()));
    }
    w_signal[i]->getRange(*var_iso,min,max);
    minimums[i] = min;
    wkspace->import(*w_gam_temp[i]);
    wkspace->import(*w_jet_temp[i]);
    wkspace->import(*w_signal[i]);
    wkspace->import(*w_leakage[i]);        
    if (mc_closure){
      wkspace->import(*w_tight_signal[i]);
      wkspace->import(*w_loose_signal[i]);
    }
  }
  w_leakage_combined = ((RooDataSet*)ws->data("leakage_combined")->Clone("leakage_combined"));
  wkspace->import(*w_leakage_combined); 

  f->Close();
  delete f;
}

void fit_leakage(string fit, TCanvas *canvas)
{
  RooDataSet *set = w_leakage_combined;
  FitInfo f; 
  if (fit == "ExpGaus"){
    f= fit_expgaus(set, "Leak", "0");
  }
  else if (fit == "Double_CBall"){  
    f = fit_2Scrystalball(set, "Leak", "0");
  }

  std::cout<<fit<<endl;
  RooPlot* frame = var_iso->frame();
  set->plotOn(frame);
  cout<<"here?"<<std::endl;
  RooAbsData* d = f.shape->generate(*var_iso,1000);
  cout<<"nope..."<<std::endl;
  RooFitResult* r = f.shape->fitTo(*d,Save()) ;
  f.shape->plotOn(frame,VisualizeError(*r,1,kFALSE),FillColor(kOrange)); 
  f.shape->plotOn(frame); 
  frame->Draw();
  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.025);
  DataText->SetNDC(kTRUE);
  DataText->DrawLatex(0.65,0.65,"Leakage Template");
  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  std::cout<<"Here?"<<endl;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  std::cout<<"Here?"<<endl;
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  std::cout<<"Here?"<<endl;
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  std::cout<<"Here?"<<endl;
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  std::cout<<"Here?"<<endl;
  }
  std::cout<<"Here?"<<endl;
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());

  canvas->Print(outputpdf.c_str());

}

//Calls fit to tight data and fills histogram with yields
void get_yields(string tight_fit, string bkg_fit, TCanvas *canvas)
{

  valerror fit_expected;
  TH1F *yields = new TH1F("h_n_bkg",(";"+label+";").c_str(),NBINS,edges);
  for (int i = 0; i < NBINS; i++){
    fit_expected = do_combined_fit(i,tight_fit,bkg_fit,canvas);
    yields->SetBinContent(i+1,fit_expected.value);
    yields->SetBinError(i+1, fit_expected.error);
  }

  TFile *fin = new TFile(histofile.c_str(),"update");
  if (fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }
  fin->cd();
  yields->Write();
  fin->Close();
  delete fin;
}

void plot_yields_histo(TCanvas *canvas)
{
  TFile *fin = new TFile(histofile.c_str(), "Read");
  TH1F *yields = (TH1F*)fin->Get("h_n_bkg");
  yields->SetMaximum(yields->GetMaximum()*1.5);
  yields->SetMinimum(-10);
  yields->Draw();

  TLegend* leg = new TLegend(0.65,0.65,.75,.75,NULL);
  leg->AddEntry(yields, "Fake #gamma","l");
  leg->SetBorderSize(0);
  leg->Draw();

  gStyle->SetOptStat(0);

  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  //string data_info = "#scale[0.8]{#font[12]{Validation}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());

  canvas->Print(outputpdf.c_str());
  fin->Close();
  delete fin;
}

void plot_yields_histo_log(TCanvas *canvas)
{
  TFile *fin = new TFile(histofile.c_str(), "Read");
  TH1F *yields = (TH1F*)fin->Get("h_n_bkg");
  canvas->SetLogy(1);
  yields->SetMaximum(yields->GetMaximum()*100);
  yields->Draw();

  TLegend* leg = new TLegend(0.65,0.65,.75,.75,NULL);
  leg->AddEntry(yields, "Fake #gamma","l");
  leg->SetBorderSize(0);
  leg->Draw();


  gStyle->SetOptStat(0);

  TLatex* DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  //string data_info = "#scale[0.8]{#font[12]{Validation}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str());

  canvas->Print(outputpdf.c_str());
  fin->Close();
  delete fin;
}

void write_yields()
{
  //Write yields
  TFile *fin = new TFile(histofile.c_str(), "Read");
  TH1F *h_n_bkg = (TH1F*)fin->Get("h_n_bkg");
  ofstream txtfile;
  txtfile.open(output_txt);
  TAxis *axis;
  txtfile<<"N_{bkg} in bins of "<<label<<endl;
  axis = h_n_bkg->GetXaxis();
  for (int i = 1; i <= h_n_bkg->GetNbinsX(); i++){
    txtfile<<"Bin: ["<<axis->GetBinLowEdge(i)<<","<<axis->GetBinUpEdge(i)<<"] Yiel\
d: "<<h_n_bkg->GetBinContent(i)<<" +/- "<<h_n_bkg->GetBinError(i)<<endl;
  }
  txtfile.close();
  fin->Close();

  return;
}

void plot_comparison(int bin, TCanvas *canvas)
{
  
  TH1 *h_template = w_nt_jet_mc[bin]->createHistogram("h_temp",*var_iso,Binning(14,-20,50));
  TH1 *h_asimov = w_loose_signal[bin]->createHistogram("h_as",*var_iso,Binning(14,-20,50));


  double chi2_result = h_template->Chi2Test(h_asimov,"WW");
  cout<<"--------Chi2 RESULT------- "<<chi2_result<<endl;


  vector<TH1*> h = {};
  gStyle->SetOptStat(0);
  canvas->SetLogy(0);
  double max = 0;
  vector<int> colors = {1,7,8,9,12,25,28,30,32,38,41,46,40};
  h.push_back(h_template);
  h.push_back(h_asimov);
  h.at(0)->SetMinimum(0);
  TH1 *histo;

  for (int i = 0; i < h.size(); i++){
    (h.at(i))->SetMarkerColor(colors.at(i));
    (h.at(i))->SetLineColor(colors.at(i));
  }
  for (int i = 0; i < h.size(); i++){
    if (i == 0){
      if (h.at(i)->GetSumOfWeights() > 0){
        histo = (h.at(i))->DrawNormalized("pe");
      }
      else{
        (h.at(i))->Draw("pe");
      }
      histo->SetMaximum(.5);
      histo->Draw("pe");
    }
    else{
      if (h.at(i)->GetSumOfWeights() > 0){
        histo = (h.at(i))->DrawNormalized("pe same");
      }
      else{
        (h.at(i))->Draw("pe same");
      }
    }
  }

  TLegend* leg = new TLegend(0.6,0.6,.9,.9,NULL);
  leg->AddEntry(h_template,"Non-Tight template");
  leg->AddEntry(h_asimov,"Tight #gamma asimov");
  leg->SetBorderSize(0);
  leg->SetNColumns(2);
  leg->Draw();
  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.025);
  DataText->SetNDC(kTRUE);
  string textchi2 = "#Chi^{2} test: "+to_string(chi2_result);
  string text_b = to_trunc_string(edges[bin])+" < "+label+" < "+to_trunc_string(edges[bin+1]);
  string text = "#splitline{"+textchi2+"}{"+text_b+"}";
  DataText->DrawLatex(0.65,0.45,text.c_str());
  canvas->Print(outputpdf.c_str());
  canvas->Clear();

}

void make_fakes_mc_histo()
{
  string range = "-20 < x && x < 2.45";
  TH1F *mc_fakes = new TH1F("h_n_bkg_mc",(";"+label+";").c_str(),NBINS,edges);
  for (int i = 0; i < NBINS; i++){
    mc_fakes->SetBinContent(i+1,w_loose_signal[i]->sumEntries(range.c_str()));
    mc_fakes->SetBinError(i+1,TMath::Sqrt(w_loose_signal[i]->sumEntries(range.c_str())));
  }



  TFile *fin = new TFile(histofile.c_str(), "Update");
  if (fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }
  fin->cd();
  mc_fakes->Write();
  fin->Close();
  delete fin;
}

void plot_closure(TCanvas *canvas)
{

  gStyle->SetOptStat(0);
  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1.0);
  pad1->SetBottomMargin(0);
  pad1->SetLogy(0);
  pad1->Draw();
  pad1->cd();

  TFile *fin = new TFile(histofile.c_str(), "Read");
  TH1F *yields = (TH1F*)fin->Get("h_n_bkg");
  TH1F *yields_mc = (TH1F*)fin->Get("h_n_bkg_mc");
  double max;
  if (yields->GetMaximum() > yields_mc->GetMaximum()){
    max = yields->GetMaximum();
  } else{
    max = yields_mc->GetMaximum();
  }
  yields->SetMaximum(max*1.5);
  yields->SetMinimum(-10);
  yields_mc->SetLineColor(7);
  yields->Draw();
  yields_mc->Draw("SAME");

  TLegend* leg = new TLegend(0.65,0.65,.75,.75,NULL);
  leg->AddEntry(yields, "Fit j -> #gamma","l");
  leg->AddEntry(yields_mc, "MC j -> #gamma","l");
  leg->SetBorderSize(0);
  leg->Draw();

  TLatex *DataText2 = new TLatex;
  DataText2->SetTextSize(0.05);
  DataText2->SetNDC(kTRUE);
  string channel_string, sample_string;
  string data_info = "#scale[1.0]{#font[12]{"+normalization+"}}";
  //string data_info = "#scale[0.8]{#font[12]{Validation}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes+"}}}";
  channel_string = "#splitline{"+channel_string+"}{#scale[0.8]{#font[12]{"+notes2+"}}}";
  string text2 = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText2->DrawLatex(0.2,0.88,text2.c_str()); canvas->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0.05,1,0.3);
  pad2->SetTopMargin(0);
  pad2->SetBottomMargin(0.3);
  pad2->SetLogy(0);
  pad2->Draw();
  pad2->cd();
  yields->GetYaxis()->SetTitle("Events");
  yields_mc->GetYaxis()->SetTitle("Ratio");

  TH1F *h_ratio = (TH1F*)yields->Clone("h_ratio");
  h_ratio->Divide(yields_mc);
  h_ratio->SetMaximum(2);
  h_ratio->SetMinimum(0);
  h_ratio->Draw();

  TAxis *axis = h_ratio->GetXaxis();
  TLine *oneline = new TLine(axis->GetBinLowEdge(1),1,axis->GetBinUpEdge(h_ratio->GetNbinsX()),1);
  oneline->Draw("SAME");

  yields->GetXaxis()->SetTitleSize(0.12);
  h_ratio->GetXaxis()->SetTitleSize(0.12);

  yields->GetXaxis()->SetLabelSize(0.50);
  h_ratio->GetXaxis()->SetLabelSize(0.10);
  yields->GetXaxis()->SetTitleOffset(1.0);
  h_ratio->GetXaxis()->SetTitleOffset(1.0);

  h_ratio->GetYaxis()->SetTitleOffset(.6);
  h_ratio->GetYaxis()->SetTitleSize(.12);


  canvas->Print(outputpdf.c_str());
}

void read_config(const char *inputConfig)
{
  TEnv config;

  config.ReadFile(inputConfig, EEnvLevel(0));
  outfile = config.GetValue("directory","None");
  
  channel = config.GetValue("channel","combined");

  normalization = config.GetValue("normalization","13 TeV 139 fb^{-1}");

  fit_range = config.GetValue("fit_range","-20 < x && x < 30");
  range_low = (config.GetValue("range_low",-20.0));
  range_high = (config.GetValue("range_high",30.0));

  LeakFit = config.GetValue("LeakFit","ExpGaus");
  TightFit = config.GetValue("TightFit","ExpGaus");
  LooseFit = config.GetValue("LooseFit","Novosibirsk");

  propagate_fit_errors = stoi(config.GetValue("Fit_Errors","0"));

  mc_closure = config.GetValue("MC_Closure",0);

  notes = config.GetValue("Plot_note1","");
  notes2 = config.GetValue("Plot_note2","");

  label = config.GetValue("Label","");

  config.Print();
}

 // Main
 int main(int argc, char **argv)
{

  if (argc < 2){
    cout<<"Invalid call to TemplateFakeDevExe"<<endl;
    cout<<"Valid call:"<<endl;
    cout<<"./TemplateFakeDevExe configfile.config"<<endl;
    return 1;
  }

  const char *inputConfig = argv[1];
  read_config(inputConfig);

  histofile = outfile+"/template_data_MC.root";
  outputpdf = outfile+"/templates.pdf";
  output_txt = outfile+"/template.txt";
  in_workspace = outfile+"/workspace_data_MC.root";
  out_workspace = outfile+"/workspace.root";
  errtxt = outfile+"/closure.txt";
  //notes = string(argv[2]);
  //notes2 = string(argv[3]);
  //notes2 = "";  

  read_in_ws();
  wkspace->Print();

  TStyle *tdrStyle = new TStyle("tdrStyle","Style for P-TDR");

  setTDRStyle( tdrStyle );

  tdrStyle->SetOptTitle(0);
  tdrStyle->SetPadBottomMargin(0.14);
  tdrStyle->SetPadLeftMargin(0.18);
  tdrStyle->SetTitleXOffset(1.0);
  tdrStyle->SetTitleYOffset(1.3);
  tdrStyle->SetNdivisions(505, "X");
  tdrStyle->SetErrorX(0.5);
  tdrStyle->SetPalette(1,0);

  tdrStyle->SetOptStat("nemr");

  TCanvas* canvas = new TCanvas("canvas","canvas",700,500);
  canvas->Print((outputpdf+"[").c_str());
  fit_leakage(LeakFit,canvas);
  for (int i = 0; i < NBINS; i++){
    plot_dataset(i, canvas);
  }

  if (mc_closure){
    for (int i = 0; i < NBINS; i++){
      plot_comparison(i,canvas);
    }
  }
  
  for (int i = 0; i < NBINS; i++){
    do_tight_fit(i,TightFit,canvas);
  }

  for (int i = 0; i < NBINS; i++){
    do_loose_fit(i,LooseFit,LeakFit,canvas);
  }

  get_yields(TightFit,LooseFit,canvas);
  plot_yields_histo(canvas);
  plot_yields_histo_log(canvas);
  write_yields();

  if (mc_closure){
    make_fakes_mc_histo();
    plot_closure(canvas);
  }
  canvas->Print((outputpdf+"]").c_str());

  wkspace->Print();
  wkspace->writeToFile(out_workspace.c_str());
        
}

