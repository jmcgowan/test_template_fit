#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <map>
#include "TFile.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TH1.h"
#include <stdio.h>
#include<cmath>

#include "TFile.h"
#include "TCanvas.h"
#include "TObject.h"
#include "TH1.h"
#include "TTree.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TTreeReader.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TChain.h"
#include<TRandom.h>
#include "TLorentzVector.h"

using namespace std;
 
#include "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/utils.C"


// GLOBAL VARIABLES

//Rebin histos
bool do_rebinning = 0;

//Uncertainty calculation: "mc" or "vary-params"
string uncertainty = "mc";

//Background subtraction
bool do_bkg_subtraction = 1;

//Global Variables
string channel;
string input_file;
string outputpdf;
string output_txt;

//histogram output
//string input_file = "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/data_02042021/ABCD_mjj/ABCD.root";

//pdf output
//string outputpdf = "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/data_02042021/ABCD_mjj/ABCD.pdf";

//text output
//string output_txt = "/afs/cern.ch/work/j/jmcgowan/analysis_scripts/fake_tools/data_02042021/ABCD_mjj/ABCD.txt";


string var = "var";
string label = "M_{jj}";
const Int_t NBINS = 10;
Double_t edges[NBINS+1] = {0,50,100,150,200,250,300,350,400,450,500};

//STRUCTURES
struct valwitherror
{
  double val;
  double sigma;
};


double solveEq(double R, double cB, double cC, double cD, double NA, double NB, double NC, double ND)
{
  double A = R*cB*cC - cD;
  double B = ND + cD*NA - R*(cC*NB+cB*NC);
  double C = R*NB*NC - NA*ND;
  double delta = sqrt((B*B)-(4*A*C));
  double NAsig = (-B+delta)/(2*A);

  return NAsig;
}

double get_uncertainty(double value, vector<double> params, vector<double> params_sigma)
{
  //gets uncertainty on value by variation of parameters
  double sigma_value = 0;
  for (int i = 0; i < params.size(); i++){
    vector<double> parameters = params;
    vector<double> parameter_err = params_sigma;
    parameters.at(i) = parameters.at(i) + parameter_err.at(i);
    double value_posvar = solveEq(parameters.at(0), parameters.at(1), parameters.at(2), parameters.at(3),
                                  parameters.at(4), parameters.at(5), parameters.at(6), parameters.at(7));
    parameters.at(i) = parameters.at(i) - (2* parameter_err.at(i));
    double value_negvar = solveEq(parameters.at(0), parameters.at(1), parameters.at(2), parameters.at(3),
                                  parameters.at(4), parameters.at(5), parameters.at(6), parameters.at(7));
    double sigma = max(abs(value_posvar - value), abs(value_negvar - value));
    sigma_value += sigma * sigma;
  }

  return sqrt(sigma_value);
}

double get_uncertainty_mc(double value, vector<double> params, vector<double> params_sigma)
{
  TH1F *h_bkg = new TH1F("h_bkg","h_bkg",20000,0, 2*value);

  TRandom *generator = new TRandom();
  int n_estimators = 10000;
  int i = 0;
  while (i < n_estimators){
    double R = generator->Gaus(params.at(0), params_sigma.at(0));
    double cB = generator->Gaus(params.at(1), params_sigma.at(1));
    double cC = generator->Gaus(params.at(2), params_sigma.at(2));
    double cD = generator->Gaus(params.at(3), params_sigma.at(3));
    double NA = generator->Gaus(params.at(4), params_sigma.at(4));
    double NB = generator->Gaus(params.at(5), params_sigma.at(5));
    double NC = generator->Gaus(params.at(6), params_sigma.at(6));
    double ND = generator->Gaus(params.at(7), params_sigma.at(7));

    double NAsig = solveEq(R, cB, cC, cD, NA, NB, NC, ND);
    h_bkg->Fill(NAsig);
    i++;
  }
  
  double uncertainty = h_bkg->GetRMS();
 
  return uncertainty;

}

vector<double> get_corr_leakage(int bin, string leakage_calc, string corr_calc)
{
  vector<double> corr_leakage;

  TFile *fin = new TFile(input_file.c_str(), "READ");
  if (fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return corr_leakage;
  }

  //read histogram

  double NA_sig = ((TH1F*)fin->Get(("h_var_A_"+corr_calc+"_mc").c_str()))->GetBinContent(bin);
  double NB_sig = ((TH1F*)fin->Get(("h_var_B_"+corr_calc+"_mc").c_str()))->GetBinContent(bin);
  double NC_sig = ((TH1F*)fin->Get(("h_var_C_"+corr_calc+"_mc").c_str()))->GetBinContent(bin);
  double ND_sig = ((TH1F*)fin->Get(("h_var_D_"+corr_calc+"_mc").c_str()))->GetBinContent(bin);
  double NAerr_sig = ((TH1F*)fin->Get(("h_var_A_"+corr_calc+"_mc").c_str()))->GetBinError(bin);
  double NBerr_sig = ((TH1F*)fin->Get(("h_var_B_"+corr_calc+"_mc").c_str()))->GetBinError(bin);
  double NCerr_sig = ((TH1F*)fin->Get(("h_var_C_"+corr_calc+"_mc").c_str()))->GetBinError(bin);
  double NDerr_sig = ((TH1F*)fin->Get(("h_var_D_"+corr_calc+"_mc").c_str()))->GetBinError(bin);

  double NA = ((TH1F*)fin->Get(("h_var_A_"+leakage_calc+"_mc").c_str()))->GetBinContent(bin);
  double NB = ((TH1F*)fin->Get(("h_var_B_"+leakage_calc+"_mc").c_str()))->GetBinContent(bin);
  double NC = ((TH1F*)fin->Get(("h_var_C_"+leakage_calc+"_mc").c_str()))->GetBinContent(bin);
  double ND = ((TH1F*)fin->Get(("h_var_D_"+leakage_calc+"_mc").c_str()))->GetBinContent(bin);
  double NAerr = ((TH1F*)fin->Get(("h_var_A_"+leakage_calc+"_mc").c_str()))->GetBinError(bin);
  double NBerr = ((TH1F*)fin->Get(("h_var_B_"+leakage_calc+"_mc").c_str()))->GetBinError(bin);
  double NCerr = ((TH1F*)fin->Get(("h_var_C_"+leakage_calc+"_mc").c_str()))->GetBinError(bin);
  double NDerr = ((TH1F*)fin->Get(("h_var_D_"+leakage_calc+"_mc").c_str()))->GetBinError(bin);

  double corr = (NA_sig*ND_sig)/(NB_sig*NC_sig);
  double cB = (NB/NA);
  double cC = (NC/NA);
  double cD = (ND/NA);
  double corr_err = TMath::Sqrt((TMath::Power((ND_sig/(NB_sig*NC_sig)),2)*TMath::Power(NAerr_sig,2))
                    +(TMath::Power((NA_sig/(NB_sig*NC_sig)),2)*TMath::Power(NDerr_sig,2))
                    +(TMath::Power((NA_sig*ND_sig)/(TMath::Power(NB_sig,2)*NC_sig),2) * TMath::Power(NBerr_sig,2))
                    +(TMath::Power((NA_sig*ND_sig)/(TMath::Power(NC_sig,2)*NB_sig),2) * TMath::Power(NCerr_sig,2)));
  double cBerr = TMath::Sqrt((TMath::Power((1/NA),2) * TMath::Power(NBerr,2))
                 +(TMath::Power((NB/TMath::Power(NA,2)),2)*TMath::Power(NAerr,2)));
  double cCerr = TMath::Sqrt((TMath::Power((1/NA),2) * TMath::Power(NCerr,2))
                 +(TMath::Power((NC/TMath::Power(NA,2)),2)*TMath::Power(NAerr,2)));
  double cDerr = TMath::Sqrt((TMath::Power((1/NA),2) * TMath::Power(NDerr,2))
                 +(TMath::Power((ND/TMath::Power(NA,2)),2)*TMath::Power(NAerr,2)));

  corr_leakage.push_back(corr);
  corr_leakage.push_back(cB);
  corr_leakage.push_back(cC);
  corr_leakage.push_back(cD);
  corr_leakage.push_back(corr_err);
  corr_leakage.push_back(cBerr);
  corr_leakage.push_back(cCerr);
  corr_leakage.push_back(cDerr);

  //cout<<"Correlation Factor R = "<<corr<<" +/- "<<corr_err<<endl;	
  return corr_leakage;

}

valwitherror compute_fakes(int bin)
{
  valwitherror n_bkg;

  //open file
  TFile *fin = new TFile(input_file.c_str(), "READ");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return n_bkg;
  }

  //read histogram
  double NA = ((TH1F*)fin->Get("h_var_A_data"))->GetBinContent(bin);
  double NB = ((TH1F*)fin->Get("h_var_B_data"))->GetBinContent(bin);
  double NC = ((TH1F*)fin->Get("h_var_C_data"))->GetBinContent(bin);
  double ND = ((TH1F*)fin->Get("h_var_D_data"))->GetBinContent(bin);
  double NAerr = ((TH1F*)fin->Get("h_var_A_data"))->GetBinError(bin);
  double NBerr = ((TH1F*)fin->Get("h_var_B_data"))->GetBinError(bin);
  double NCerr = ((TH1F*)fin->Get("h_var_C_data"))->GetBinError(bin);
  double NDerr = ((TH1F*)fin->Get("h_var_D_data"))->GetBinError(bin);

  fin->Close();
  
  vector<double> corr_leakage = get_corr_leakage(bin, "leakage","corr");
  double R = corr_leakage.at(0);
  double cB = corr_leakage.at(1);
  double cC = corr_leakage.at(2);
  double cD = corr_leakage.at(3);
  double Rerr = corr_leakage.at(4);
  double cBerr = corr_leakage.at(5);
  double cCerr = corr_leakage.at(6);
  double cDerr = corr_leakage.at(7);

  double NAsig = solveEq(R,cB,cC,cD,NA,NB,NC,ND);
  vector<double> parameters = {R,cB,cC,cD,NA,NB,NC,ND};
  vector<double> parameter_err = {Rerr,cBerr,cCerr,cDerr,NAerr,NBerr,NCerr,NDerr};
  double NAsig_error;
  if (uncertainty == "vary-params"){
    NAsig_error = get_uncertainty(NAsig,parameters,parameter_err);
  }
  else if (uncertainty == "mc"){
    NAsig_error = get_uncertainty_mc(NAsig,parameters,parameter_err);
  }

  n_bkg.val = NA - NAsig;
  n_bkg.sigma = TMath::Sqrt(TMath::Power(NAerr,2) + TMath::Power(NAsig_error,2));
  return n_bkg;
  
}


void rebin_histos(int bin)
{
  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }


  TH1F *h_A_data = (TH1F*)fin->Get("h_var_A_data");
  TH1F *h_B_data = (TH1F*)fin->Get("h_var_B_data");
  TH1F *h_C_data = (TH1F*)fin->Get("h_var_C_data");
  TH1F *h_D_data = (TH1F*)fin->Get("h_var_D_data");
  TH1F *h_A_leakage_mc = (TH1F*)fin->Get("h_var_A_leakage_mc");
  TH1F *h_B_leakage_mc = (TH1F*)fin->Get("h_var_B_leakage_mc");
  TH1F *h_C_leakage_mc = (TH1F*)fin->Get("h_var_C_leakage_mc");
  TH1F *h_D_leakage_mc = (TH1F*)fin->Get("h_var_D_leakage_mc");
  TH1F *h_A_corr_mc = (TH1F*)fin->Get("h_var_A_corr_mc");
  TH1F *h_B_corr_mc = (TH1F*)fin->Get("h_var_B_corr_mc");
  TH1F *h_C_corr_mc = (TH1F*)fin->Get("h_var_C_corr_mc");
  TH1F *h_D_corr_mc = (TH1F*)fin->Get("h_var_D_corr_mc");
  TH1F *h_R = (TH1F*)fin->Get("h_R");
  TH1F *h_cB = (TH1F*)fin->Get("h_cB");
  TH1F *h_cC = (TH1F*)fin->Get("h_cC");
  TH1F *h_cD = (TH1F*)fin->Get("h_cD");
  TH1F *h_A_had_mc = (TH1F*)fin->Get("h_var_had_A_mc");
  TH1F *h_B_had_mc = (TH1F*)fin->Get("h_var_had_B_mc");
  TH1F *h_C_had_mc = (TH1F*)fin->Get("h_var_had_C_mc");
  TH1F *h_D_had_mc = (TH1F*)fin->Get("h_var_had_D_mc");
  TH1F *h_A_wjets_WVBF_mc = (TH1F*)fin->Get("h_var_wjets_WVBF_A_mc");
  TH1F *h_B_wjets_WVBF_mc = (TH1F*)fin->Get("h_var_wjets_WVBF_B_mc");
  TH1F *h_C_wjets_WVBF_mc = (TH1F*)fin->Get("h_var_wjets_WVBF_C_mc");
  TH1F *h_D_wjets_WVBF_mc = (TH1F*)fin->Get("h_var_wjets_WVBF_D_mc");
  TH1F *h_n_bkg = (TH1F*)fin->Get("h_n_bkg");


  //Do rebinning here
  vector<double> edges_temp;
  TAxis *axis = h_R->GetXaxis();
  edges_temp.push_back(axis->GetBinLowEdge(1));
  for (int i = 1; i <= h_R->GetNbinsX(); i++){
    if (i != bin){
      edges_temp.push_back(axis->GetBinUpEdge(i));
    }
    if ((i == bin) && (bin == h_R->GetNbinsX())){
      if (edges_temp.size() > 1){
        edges_temp.pop_back();
      }
      edges_temp.push_back(axis->GetBinUpEdge(i));
    }
  }

  double xbins[edges_temp.size()];
  for (int i = 0; i<edges_temp.size(); i++){
    xbins[i] = edges_temp.at(i);
  }

  h_A_data = (TH1F*)h_A_data->Rebin(edges_temp.size()-1,"",xbins);
  h_B_data = (TH1F*)h_B_data->Rebin(edges_temp.size()-1,"",xbins);
  h_C_data = (TH1F*)h_C_data->Rebin(edges_temp.size()-1,"",xbins);
  h_D_data = (TH1F*)h_D_data->Rebin(edges_temp.size()-1,"",xbins);
  h_A_leakage_mc = (TH1F*)h_A_leakage_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_B_leakage_mc = (TH1F*)h_B_leakage_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_C_leakage_mc = (TH1F*)h_C_leakage_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_D_leakage_mc = (TH1F*)h_D_leakage_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_A_corr_mc = (TH1F*)h_A_corr_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_B_corr_mc = (TH1F*)h_B_corr_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_C_corr_mc = (TH1F*)h_C_corr_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_D_corr_mc = (TH1F*)h_D_corr_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_R = (TH1F*)h_R->Rebin(edges_temp.size()-1,"",xbins);
  h_cB = (TH1F*)h_cB->Rebin(edges_temp.size()-1,"",xbins);
  h_cC = (TH1F*)h_cC->Rebin(edges_temp.size()-1,"",xbins);
  h_cD = (TH1F*)h_cD->Rebin(edges_temp.size()-1,"",xbins);
  h_A_had_mc = (TH1F*)h_A_had_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_B_had_mc = (TH1F*)h_B_had_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_C_had_mc = (TH1F*)h_C_had_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_D_had_mc = (TH1F*)h_D_had_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_A_wjets_WVBF_mc = (TH1F*)h_A_wjets_WVBF_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_B_wjets_WVBF_mc = (TH1F*)h_B_wjets_WVBF_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_C_wjets_WVBF_mc = (TH1F*)h_C_wjets_WVBF_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_D_wjets_WVBF_mc = (TH1F*)h_D_wjets_WVBF_mc->Rebin(edges_temp.size()-1,"",xbins);
  h_n_bkg = (TH1F*)h_n_bkg->Rebin(edges_temp.size()-1,"",xbins);

  h_A_data->Write(0, TObject::kWriteDelete);
  h_B_data->Write(0, TObject::kWriteDelete);
  h_C_data->Write(0, TObject::kWriteDelete);
  h_D_data->Write(0, TObject::kWriteDelete);
  h_A_had_mc->Write(0, TObject::kWriteDelete);
  h_B_had_mc->Write(0, TObject::kWriteDelete);
  h_C_had_mc->Write(0, TObject::kWriteDelete);
  h_D_had_mc->Write(0, TObject::kWriteDelete);
  h_A_wjets_WVBF_mc->Write(0, TObject::kWriteDelete);
  h_B_wjets_WVBF_mc->Write(0, TObject::kWriteDelete);
  h_C_wjets_WVBF_mc->Write(0, TObject::kWriteDelete);
  h_D_wjets_WVBF_mc->Write(0, TObject::kWriteDelete);
  h_A_leakage_mc->Write(0, TObject::kWriteDelete);
  h_B_leakage_mc->Write(0, TObject::kWriteDelete);
  h_C_leakage_mc->Write(0, TObject::kWriteDelete);
  h_D_leakage_mc->Write(0, TObject::kWriteDelete);
  h_A_corr_mc->Write(0, TObject::kWriteDelete);
  h_B_corr_mc->Write(0, TObject::kWriteDelete);
  h_C_corr_mc->Write(0, TObject::kWriteDelete);
  h_D_corr_mc->Write(0, TObject::kWriteDelete);
  h_R->Write(0, TObject::kWriteDelete);
  h_cB->Write(0, TObject::kWriteDelete);
  h_cC->Write(0, TObject::kWriteDelete);
  h_cD->Write(0, TObject::kWriteDelete);
  h_n_bkg->Write(0, TObject::kWriteDelete);

  fin->Close();  

}

void make_histos()
{

  int nbins = NBINS;
  int i = 1;
  int nbins_temp = nbins;
  int min_bin = 20;
  int current_bin = 20;
  vector<double> edges;
  while (i <= nbins){
    valwitherror n_bkg = compute_fakes(i);
    vector<double> corr_leakage = get_corr_leakage(i, "leakage","corr");
    if ((do_rebinning) && 
       ((TMath::Abs(n_bkg.sigma/n_bkg.val) > 1.0) || 
       (n_bkg.val < 0) ||
       (current_bin < min_bin) ||
       (n_bkg.val != n_bkg.val) || (n_bkg.sigma != n_bkg.sigma) ||
       (corr_leakage.at(0) != corr_leakage.at(0)) || 
       (corr_leakage.at(0) == 0) ||
       (std::isinf(n_bkg.val)) || (std::isinf(n_bkg.sigma)))){ 
      rebin_histos(i);
      nbins_temp = nbins - 1;
      current_bin += 20; 
    }  
    if (nbins_temp == nbins || i == nbins){
      min_bin = current_bin;
      current_bin = 20;
      TFile *fin = new TFile(input_file.c_str(), "Update");
      TH1F *h_R = (TH1F*)fin->Get("h_R");
      TH1F *h_n_bkg = (TH1F*)fin->Get("h_n_bkg");
      TH1F *h_cB = (TH1F*)fin->Get("h_cB");
      TH1F *h_cC = (TH1F*)fin->Get("h_cC");
      TH1F *h_cD = (TH1F*)fin->Get("h_cD");
      h_R->SetBinContent(i,corr_leakage.at(0));
      h_R->SetBinError(i,corr_leakage.at(4));
      h_cB->SetBinContent(i,corr_leakage.at(1));
      h_cB->SetBinError(i,corr_leakage.at(5));
      h_cC->SetBinContent(i,corr_leakage.at(2));
      h_cC->SetBinError(i,corr_leakage.at(6));
      h_cD->SetBinContent(i,corr_leakage.at(3));
      h_cD->SetBinError(i,corr_leakage.at(7));
      h_n_bkg->SetBinContent(i,n_bkg.val);
      h_n_bkg->SetBinError(i,n_bkg.sigma);
      fin->cd();
      h_n_bkg->Write(0, TObject::kWriteDelete);
      h_R->Write(0, TObject::kWriteDelete);
      h_cB->Write(0, TObject::kWriteDelete);
      h_cC->Write(0, TObject::kWriteDelete);
      h_cD->Write(0, TObject::kWriteDelete);
      fin->Close();
      i++;
    }
    nbins = nbins_temp;
  }

  return;
}

void write_yields()
{
  //Write yields
  TFile *fin = new TFile(input_file.c_str(), "Read");
  TH1F *h_n_bkg = (TH1F*)fin->Get("h_n_bkg");
  ofstream txtfile;
  txtfile.open(output_txt);
  TAxis *axis;
  txtfile<<"N_{bkg} in bins of var"<<endl;
  axis = h_n_bkg->GetXaxis();
  for (int i = 1; i <= h_n_bkg->GetNbinsX(); i++){
    txtfile<<"Bin: ["<<axis->GetBinLowEdge(i)<<","<<axis->GetBinUpEdge(i)<<"] Yield: "<<
          h_n_bkg->GetBinContent(i)<<" +/- "<<h_n_bkg->GetBinError(i)<<endl;
  }
  txtfile.close();
  fin->Close();

  return;  
}

void subtract_backgrounds()
{

  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  TH1F *h_A_data = (TH1F*)fin->Get("h_var_A_data");
  TH1F *h_B_data = (TH1F*)fin->Get("h_var_B_data");
  TH1F *h_C_data = (TH1F*)fin->Get("h_var_C_data");
  TH1F *h_D_data = (TH1F*)fin->Get("h_var_D_data");
  TH1F *h_A_mc = (TH1F*)fin->Get("h_var_A_mc");
  TH1F *h_B_mc = (TH1F*)fin->Get("h_var_B_mc");
  TH1F *h_C_mc = (TH1F*)fin->Get("h_var_C_mc");
  TH1F *h_D_mc = (TH1F*)fin->Get("h_var_D_mc");

  //h_A_data->Add(h_A_mc,-1);
  h_B_data->Add(h_B_mc,-1);
  h_C_data->Add(h_C_mc,-1);
  h_D_data->Add(h_D_mc,-1);

  h_A_data->Write(0, TObject::kWriteDelete);
  h_B_data->Write(0, TObject::kWriteDelete);
  h_C_data->Write(0, TObject::kWriteDelete);
  h_D_data->Write(0, TObject::kWriteDelete);

  fin->Close();
  return;
}

void draw_plot(TH1F* h1, const char* outfile, TCanvas* canvas)
{
  gStyle->SetOptStat(0);
  canvas->SetLogy(0);

  h1->SetMarkerColor(9);
  h1->SetLineColor(9);
  h1->SetMaximum(h1->GetMaximum() * 2);

  h1->Draw("pe");

  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.05);
  DataText->SetNDC(kTRUE);
  string channel_string;
  string data_info = "#scale[0.8]{#font[12]{13 TeV 36 fb^{-1}}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  string text = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText->DrawLatex(0.2,0.88,text.c_str());

  canvas->Print(outfile);
  canvas->Clear();
}

void draw_multiple_plots(vector<TH1F*> h, vector<string> nameList, const char* outfile, TCanvas* canvas)
{
  gStyle->SetOptStat(0);
  canvas->SetLogy(1);
  //canvas->SetLogx(1);
  double max = 0;
  vector<int> colors = {1,8,9,12,28,38,41,46};
  vector<int> styles = {1,2,5,3,25,26,27,33};

  for ( int i = 0; i < h.size(); i++){
    if ((h.at(i))->GetMaximum() > max){
      max = ((h.at(i))->GetMaximum())*1000;
    }
  }

  for ( int i = 0; i < h.size(); i++){
    (h.at(i))->SetMaximum(max);
    (h.at(i))->SetMinimum(1);
    //((h.at(i))->GetXaxis())->SetRangeUser(15,70);
    (h.at(i))->SetMarkerColor(colors.at(i));
    (h.at(i))->SetMarkerStyle(styles.at(i));
    (h.at(i))->SetLineColor(colors.at(i));
    if (i == 0){
      (h.at(i))->Draw("pe");
    }
    else{
      (h.at(i))->Draw("pe same");
    }
  }

  TLegend* leg = new TLegend(0.7,0.8,.9,.9,NULL);
  for (int i = 0; i< h.size(); i++){
    leg->AddEntry(h.at(i), (nameList.at(i)).c_str(),"l");
  }
  leg->Draw();

  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.05);
  DataText->SetNDC(kTRUE);
  string channel_string;
  string data_info = "#scale[0.8]{#font[12]{13 TeV 36 fb^{-1}}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  string text = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText->DrawLatex(0.2,0.88,text.c_str());

  canvas->Print(outfile);
  canvas->Clear();
}

void draw_multiple_plots2(vector<TH1F*> h, vector<string> nameList, const char* outfile, TCanvas* canvas, bool log)
{
  gStyle->SetOptStat(0);
  canvas->SetLogy(log);
  double max = -10;
  double min = 10;
  vector<int> colors = {1,8,9,12,28,38,41,46};
  vector<int> styles = {26,3,32,1,2,5,3,25,26,27,33};
  for (int i = 0; i < h.size(); i++){
    if (h.at(i)->GetMaximum() > max){
      max = h.at(i)->GetMaximum();
    }
    if (h.at(i)->GetMinimum() < min){
      min = h.at(i)->GetMinimum();
    }
  }
  if (log){
    if (min <= 0){
      min = .001;
    }
    max = max * 10;
    min = min * .1;
  }
  else{
    min = min -1;
    max = max + 1;
  }

  for ( int i = 0; i < h.size(); i++){
    (h.at(i))->SetMaximum(max);
    (h.at(i))->SetMinimum(min);
    (h.at(i))->SetMarkerColor(colors.at(i));
    (h.at(i))->SetMarkerStyle(styles.at(i));
    (h.at(i))->SetLineColor(colors.at(i));
    if (i == 0){
      (h.at(i))->Draw("pe");
    }
    else{
      (h.at(i))->Draw("pe same");
    }
  }

  TLegend* leg = new TLegend(0.7,0.8,.9,.9,NULL);
  for (int i = 0; i< h.size(); i++){
    leg->AddEntry(h.at(i), (nameList.at(i)).c_str(),"l");
  }
  leg->Draw();

  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.05);
  DataText->SetNDC(kTRUE);
  string channel_string;
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  string text = "#splitline{ATLAS Internal}{"+channel_string+"}";
  DataText->DrawLatex(0.2,0.88,text.c_str());

  canvas->Print(outfile);
  canvas->Clear();
}

void draw_ratio_plot(vector<TH1F*> h, vector<string> nameList, const char* outfile, TCanvas* canvas)
{
  gStyle->SetOptStat(0);

  vector<int> colors = {1,7,8,9,12,25,28,30,32,38,41,46,40};

  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1.0);
  pad1->SetBottomMargin(0);
  pad1->SetLogy(1);
  pad1->Draw();
  pad1->cd();
  double max = 0;


  for (int i = 0; i < h.size(); i++){
    if ((h.at(i))->GetMaximum() > max){
      max = ((h.at(i))->GetMaximum())*10;
      (h.at(i))->SetMaximum(max);
    }
    (h.at(i))->SetMarkerColor(colors.at(i));
    (h.at(i))->SetLineColor(colors.at(i));
    if (i == 0){
      (h.at(i))->Draw("*");
    }
    else{
      (h.at(i))->Draw("same *");
    }
  }

  TLegend* leg = new TLegend(0.7,0.8,.9,.9,NULL);
  for (int i = 0; i< h.size(); i++){
    leg->AddEntry(h.at(i), (nameList.at(i)).c_str(),"l");
  }
  leg->Draw();

  TLatex* DataText = new TLatex;
  DataText->SetTextSize(0.05);
  DataText->SetNDC(kTRUE);
  string channel_string;
  string data_info = "#scale[0.8]{#font[12]{13 TeV 139 fb^{-1}}}";
  if (channel == "combined"){
    channel_string = "#scale[0.8]{#font[12]{e + #mu channel}}";
  }
  else if (channel == "electron"){
    channel_string = "#scale[0.8]{#font[12]{e channel}}";
  }
  else{
    channel_string = "#scale[0.8]{#font[12]{#mu channel}}";
  }
  string text = "#splitline{ATLAS Internal}{#splitline{"+data_info+"}{"+channel_string+"}}";
  DataText->DrawLatex(0.2,0.88,text.c_str());


  canvas->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0.05,1,0.3);
  pad2->SetTopMargin(0);
  pad2->SetBottomMargin(0.3);
  pad2->SetLogy(0);
  pad2->Draw();
  pad2->cd();
  h.at(0)->GetYaxis()->SetTitle("Events");
  h.at(1)->GetYaxis()->SetTitle("ratio");

  TH1F* h_ratio;
  max = 0;
  //for (int i = 1; i < h.size(); i++){
  //h_ratio.push_back((TH1F*)h.at(1)->Clone((("h"+to_string(1)).c_str())));
  //h_ratio.at(0)->Divide(h.at(0));
  //  if ((h_ratio.at(i-1))->GetMaximum() > max){
  //    max = ((h_ratio.at(i-1))->GetMaximum())*2;
  //    (h_ratio.at(i-1))->SetMaximum(max);
  //  }
  //  h_ratio.at(i-1)->Draw("p");
  //}
  h_ratio = (TH1F*)h.at(1)->Clone("h1");
  h_ratio->Divide(h.at(0));
  h_ratio->Draw("p");

  h.at(0)->GetXaxis()->SetTitleSize(0.12);
  h_ratio->GetXaxis()->SetTitleSize(0.12);

  h.at(0)->GetXaxis()->SetLabelSize(0.50);
  h_ratio->GetXaxis()->SetLabelSize(0.10);
  h.at(0)->GetXaxis()->SetTitleOffset(1.0);
  h_ratio->GetXaxis()->SetTitleOffset(1.0);

  h_ratio->GetYaxis()->SetTitleOffset(.6);
  h_ratio->GetYaxis()->SetTitleSize(.12);


  canvas->Print(outfile);
  canvas->Clear();
}

void make_plots()
{
  TCanvas* canvas = new TCanvas("canvas","canvas");
  canvas->Print((outputpdf+"[").c_str());

  //open file
  TFile *fin = new TFile(input_file.c_str(), "READ");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }
  // Fakes distribution and MC hadronic truth type photons
  TH1F *h1, *h2, *h3, *h4, *h5, *h6;
  string name;
  h1 = (TH1F*)fin->Get("h_n_bkg");
  h2 = (TH1F*)fin->Get("h_var_had_A_mc");
  h3 = (TH1F*)fin->Get("h_var_wjets_WVBF_A_mc");
  draw_ratio_plot({h1,h2,h3},{"N_{bkg}","Hadronic MC","Wjets + WVBF MC"},outputpdf.c_str(),canvas);
  
  h1 = (TH1F*)fin->Get("h_var_A_data");
  h2 = (TH1F*)fin->Get("h_var_B_data");
  h3 = (TH1F*)fin->Get("h_var_C_data");
  h4 = (TH1F*)fin->Get("h_var_D_data");
  draw_multiple_plots({h1,h2,h3,h4},{"A","B","C","D"},outputpdf.c_str(),canvas);

  //LEAKAGE plots
  vector<string> regions = {"B","C","D"};
  for (int i = 0; i < regions.size(); i++){
    h1 = (TH1F*)fin->Get(("h_c"+regions.at(i)+"_signal_var").c_str());
    h2 = (TH1F*)fin->Get(("h_c"+regions.at(i)+"_qcd_var").c_str());
    h3 = (TH1F*)fin->Get(("h_c"+regions.at(i)+"_zgamma_sig_var").c_str());
    //h4 = (TH1F*)fin->Get(("h_c"+regions.at(i)+"_ZVBF_sig_var").c_str());
    //h5 = (TH1F*)fin->Get(("h_c"+regions.at(i)+"_WVBF_sig_var").c_str());
    h6 = (TH1F*)fin->Get(("h_c"+regions.at(i)).c_str());
    draw_multiple_plots2({h1,h2,h3,h6},{"EWK W#gamma","QCD W#gamma","Z#gamma","Combined"},outputpdf.c_str(),canvas,1);
  }
  //Correlation plots
  h1 = (TH1F*)fin->Get("h_R");
  h2 = (TH1F*)fin->Get("h_corr_wjets_had");
  //  h3 = (TH1F*)fin->Get("h_corr_zjets_had");
  h4 = (TH1F*)fin->Get("h_corr_ttbar_had");
  draw_multiple_plots2({h1,h2,h4},{"Combined","W+jets","ttbar"},outputpdf.c_str(),canvas,0);
  canvas->Print((outputpdf+"]").c_str());
}

void create_combined_mc(vector<string> backgrounds)
{

  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  string title = ";"+label+";";
  TH1F *h_A = new TH1F("h_var_A_mc",title.c_str(), NBINS, edges);
  TH1F *h_B = new TH1F("h_var_B_mc",title.c_str(), NBINS, edges);
  TH1F *h_C = new TH1F("h_var_C_mc",title.c_str(), NBINS, edges);
  TH1F *h_D = new TH1F("h_var_D_mc",title.c_str(), NBINS, edges);
 
  for (int i = 0; i < backgrounds.size(); i++){
    TH1F *h_bkg_A = (TH1F*)fin->Get(("h_var_A_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_B = (TH1F*)fin->Get(("h_var_B_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_C = (TH1F*)fin->Get(("h_var_C_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_D = (TH1F*)fin->Get(("h_var_D_"+backgrounds.at(i)+"_mc").c_str());
    h_A->Add(h_bkg_A,1);
    h_B->Add(h_bkg_B,1);
    h_C->Add(h_bkg_C,1);
    h_D->Add(h_bkg_D,1);
  }

  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();

}


void create_combined_had(vector<string> backgrounds)
{

  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  string title = ";"+label+";";
  TH1F *h_A = new TH1F("h_var_had_A_mc",title.c_str(), NBINS, edges);
  TH1F *h_B = new TH1F("h_var_had_B_mc",title.c_str(), NBINS, edges);
  TH1F *h_C = new TH1F("h_var_had_C_mc",title.c_str(), NBINS, edges);
  TH1F *h_D = new TH1F("h_var_had_D_mc",title.c_str(), NBINS, edges);
 
  for (int i = 0; i < backgrounds.size(); i++){
    TH1F *h_bkg_A = (TH1F*)fin->Get(("h_var_A_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_B = (TH1F*)fin->Get(("h_var_B_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_C = (TH1F*)fin->Get(("h_var_C_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_D = (TH1F*)fin->Get(("h_var_D_"+backgrounds.at(i)+"_mc").c_str());
    h_A->Add(h_bkg_A,1);
    h_B->Add(h_bkg_B,1);
    h_C->Add(h_bkg_C,1);
    h_D->Add(h_bkg_D,1);
  }

  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();

}


void create_combined_wjets_bkg(vector<string> backgrounds)
{

  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  string title = ";"+label+";";
  TH1F *h_A = new TH1F("h_var_wjets_WVBF_A_mc",title.c_str(), NBINS, edges);
  TH1F *h_B = new TH1F("h_var_wjets_WVBF_B_mc",title.c_str(), NBINS, edges);
  TH1F *h_C = new TH1F("h_var_wjets_WVBF_C_mc",title.c_str(), NBINS, edges);
  TH1F *h_D = new TH1F("h_var_wjets_WVBF_D_mc",title.c_str(), NBINS, edges);
 
  for (int i = 0; i < backgrounds.size(); i++){
    TH1F *h_bkg_A = (TH1F*)fin->Get(("h_var_A_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_B = (TH1F*)fin->Get(("h_var_B_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_C = (TH1F*)fin->Get(("h_var_C_"+backgrounds.at(i)+"_mc").c_str());
    TH1F *h_bkg_D = (TH1F*)fin->Get(("h_var_D_"+backgrounds.at(i)+"_mc").c_str());
    h_A->Add(h_bkg_A,1);
    h_B->Add(h_bkg_B,1);
    h_C->Add(h_bkg_C,1);
    h_D->Add(h_bkg_D,1);
  }

  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();

}

void create_combined_leakage_mc(vector<string> samples)
{

  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  string title = ";"+label+";";
  TH1F *h_A = new TH1F("h_var_A_leakage_mc",title.c_str(), NBINS, edges);
  TH1F *h_B = new TH1F("h_var_B_leakage_mc",title.c_str(), NBINS, edges);
  TH1F *h_C = new TH1F("h_var_C_leakage_mc",title.c_str(), NBINS, edges);
  TH1F *h_D = new TH1F("h_var_D_leakage_mc",title.c_str(), NBINS, edges);
 
  for (int i = 0; i < samples.size(); i++){
    TH1F *h_bkg_A = (TH1F*)fin->Get(("h_var_A_"+samples.at(i)+"_sig_mc").c_str());
    TH1F *h_bkg_B = (TH1F*)fin->Get(("h_var_B_"+samples.at(i)+"_sig_mc").c_str());
    TH1F *h_bkg_C = (TH1F*)fin->Get(("h_var_C_"+samples.at(i)+"_sig_mc").c_str());
    TH1F *h_bkg_D = (TH1F*)fin->Get(("h_var_D_"+samples.at(i)+"_sig_mc").c_str());
    h_A->Add(h_bkg_A,1);
    h_B->Add(h_bkg_B,1);
    h_C->Add(h_bkg_C,1);
    h_D->Add(h_bkg_D,1);
  }

  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();

}


void create_combined_correlation_mc(vector<string> samples)
{

  //open file
  TFile *fin = new TFile(input_file.c_str(), "UPDATE");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  string title = ";"+label+";";
  TH1F *h_A = new TH1F("h_var_A_corr_mc",title.c_str(), NBINS, edges);
  TH1F *h_B = new TH1F("h_var_B_corr_mc",title.c_str(), NBINS, edges);
  TH1F *h_C = new TH1F("h_var_C_corr_mc",title.c_str(), NBINS, edges);
  TH1F *h_D = new TH1F("h_var_D_corr_mc",title.c_str(), NBINS, edges);
 
  for (int i = 0; i < samples.size(); i++){
    TH1F *h_bkg_A = (TH1F*)fin->Get(("h_var_A_"+samples.at(i)+"_had_mc").c_str());
    TH1F *h_bkg_B = (TH1F*)fin->Get(("h_var_B_"+samples.at(i)+"_had_mc").c_str());
    TH1F *h_bkg_C = (TH1F*)fin->Get(("h_var_C_"+samples.at(i)+"_had_mc").c_str());
    TH1F *h_bkg_D = (TH1F*)fin->Get(("h_var_D_"+samples.at(i)+"_had_mc").c_str());
    h_A->Add(h_bkg_A,1);
    h_B->Add(h_bkg_B,1);
    h_C->Add(h_bkg_C,1);
    h_D->Add(h_bkg_D,1);
  }

  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();


}


void initialize_histograms()
{

  TFile *fin = new TFile(input_file.c_str(), "Update");
  TH1F *h_n_bkg = new TH1F("h_n_bkg",(";"+label+";N^{bkg}_{A}").c_str(),NBINS,edges);
  TH1F *h_purity = new TH1F("h_purity",(";"+label+";Photon purity").c_str(),NBINS,edges);
  TH1F *h_cA = new TH1F("h_cA",(";"+label+";c_{B}").c_str(),NBINS,edges);
  TH1F *h_cB = new TH1F("h_cB",(";"+label+";c_{B}").c_str(),NBINS,edges);
  TH1F *h_cC = new TH1F("h_cC",(";"+label+";c_{C}").c_str(),NBINS,edges);
  TH1F *h_cD = new TH1F("h_cD",(";"+label+";c_{D}").c_str(),NBINS,edges);
  TH1F *h_R = new TH1F("h_R",(";"+label+";R").c_str(),NBINS,edges);

  fin->cd();
  h_n_bkg->Write(0, TObject::kWriteDelete);
  h_cA->Write(0,TObject::kWriteDelete);
  h_cB->Write(0, TObject::kWriteDelete);
  h_cC->Write(0, TObject::kWriteDelete);
  h_cD->Write(0, TObject::kWriteDelete);
  h_R->Write(0, TObject::kWriteDelete);
  fin->Close();

}

void make_leakage_histo(string name)
{
  TFile *fin = new TFile(input_file.c_str(), "Update");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }


  TH1F *h_A_data = (TH1F*)fin->Get("h_var_A_data");
  TH1F *h_A = (TH1F*)fin->Get(("h_var_A_"+name+"_mc").c_str());
  TH1F *h_B = (TH1F*)fin->Get(("h_var_B_"+name+"_mc").c_str());
  TH1F *h_C = (TH1F*)fin->Get(("h_var_C_"+name+"_mc").c_str());
  TH1F *h_D = (TH1F*)fin->Get(("h_var_D_"+name+"_mc").c_str());

  //Get bins
  vector<double> edges_temp;
  TAxis *axis = h_A_data->GetXaxis();
  edges_temp.push_back(axis->GetBinLowEdge(1));
  for (int i = 1; i <= h_A_data->GetNbinsX(); i++){
    edges_temp.push_back(axis->GetBinUpEdge(i));
  }

  double xbins[edges_temp.size()];
  for (int i = 0; i<edges_temp.size(); i++){
    xbins[i] = edges_temp.at(i);
  }

  h_A = (TH1F*)h_A->Rebin(edges_temp.size()-1,"",xbins);
  h_B = (TH1F*)h_B->Rebin(edges_temp.size()-1,"",xbins);
  h_C = (TH1F*)h_C->Rebin(edges_temp.size()-1,"",xbins);
  h_D = (TH1F*)h_D->Rebin(edges_temp.size()-1,"",xbins);

  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();

  string title = ";"+label+";R";

  TH1F* h_cB = new TH1F(("h_cB_"+name+"_"+var).c_str(),(";"+title+";cB").c_str(),edges_temp.size()-1,xbins);
  TH1F* h_cC = new TH1F(("h_cC_"+name+"_"+var).c_str(),(";"+title+";cC").c_str(),edges_temp.size()-1,xbins);
  TH1F* h_cD = new TH1F(("h_cD_"+name+"_"+var).c_str(),(";"+title+";cD").c_str(),edges_temp.size()-1,xbins);

  for (int bin = 1; bin <= h_cB->GetNbinsX(); bin++){
    vector<double> corr_leakage = get_corr_leakage(bin, name,name);
    h_cB->SetBinContent(bin,corr_leakage.at(1));
    h_cB->SetBinError(bin,corr_leakage.at(5));
    h_cC->SetBinContent(bin,corr_leakage.at(2));
    h_cC->SetBinError(bin,corr_leakage.at(6));
    h_cD->SetBinContent(bin,corr_leakage.at(3));
    h_cD->SetBinError(bin,corr_leakage.at(7));
  }
  fin = new TFile(input_file.c_str(), "Update");
  fin->cd();
  h_cB->Write(0, TObject::kWriteDelete);
  h_cC->Write(0, TObject::kWriteDelete);
  h_cD->Write(0, TObject::kWriteDelete);
  fin->Close();
}

void make_corr_histo(string name)
{
  TFile *fin = new TFile(input_file.c_str(), "update");
  if(fin == nullptr){
    cout<<"Error loading file, exiting"<<endl;
    return;
  }

  TH1F *h_A_data = (TH1F*)fin->Get("h_var_A_data");
  TH1F *h_A = (TH1F*)fin->Get(("h_var_A_"+name+"_mc").c_str());
  TH1F *h_B = (TH1F*)fin->Get(("h_var_B_"+name+"_mc").c_str());
  TH1F *h_C = (TH1F*)fin->Get(("h_var_C_"+name+"_mc").c_str());
  TH1F *h_D = (TH1F*)fin->Get(("h_var_D_"+name+"_mc").c_str());

  //Get bins
  vector<double> edges_temp;
  TAxis *axis = h_A_data->GetXaxis();
  edges_temp.push_back(axis->GetBinLowEdge(1));
  for (int i = 1; i <= h_A_data->GetNbinsX(); i++){
    edges_temp.push_back(axis->GetBinUpEdge(i));
  }
  double xbins[edges_temp.size()];
  for (int i = 0; i<edges_temp.size(); i++){
    xbins[i] = edges_temp.at(i);
  }
  h_A = (TH1F*)h_A->Rebin(edges_temp.size()-1,"",xbins);
  h_B = (TH1F*)h_B->Rebin(edges_temp.size()-1,"",xbins);
  h_C = (TH1F*)h_C->Rebin(edges_temp.size()-1,"",xbins);
  h_D = (TH1F*)h_D->Rebin(edges_temp.size()-1,"",xbins);
  fin->cd();
  h_A->Write(0, TObject::kWriteDelete);
  h_B->Write(0, TObject::kWriteDelete);
  h_C->Write(0, TObject::kWriteDelete);
  h_D->Write(0, TObject::kWriteDelete);
  fin->Close();
  string title = ";"+label+";R";
  TH1F* h_corr = new TH1F(("h_corr_"+name).c_str(),title.c_str(),edges_temp.size()-1,xbins);

  for (int bin = 1; bin <= h_corr->GetNbinsX(); bin++){
    vector<double> corr_leakage = get_corr_leakage(bin,name,name);
    h_corr->SetBinContent(bin,corr_leakage.at(0));
    h_corr->SetBinError(bin,corr_leakage.at(4));
  }

  fin = new TFile(input_file.c_str(), "update");
  fin->cd();
  h_corr->Write(0, TObject::kWriteDelete);
  fin->Close();
}

int main()
{

  vector<string> backgrounds = {"wjets","zjets","zgamma","WVBF","ZVBF","singletop","ttbar","ttgamma","tWgamma"};
  vector<string> samples_leakage = {"qcd","signal"};
  vector<string> samples_corr = {"signal","qcd","wjets","zjets","ttbar","singletop","tWgamma","ttgamma","zgamma","WVBF","ZVBF"};
  create_combined_mc(backgrounds);
  create_combined_wjets_bkg({"wjets","WVBF"});
  create_combined_had(samples_corr);
  create_combined_leakage_mc(samples_leakage);
  create_combined_correlation_mc(samples_corr);
  if (do_bkg_subtraction){
    subtract_backgrounds();
  }
  make_histos();
  write_yields();
  make_corr_histo("signal_had");
  make_corr_histo("qcd_had");
  make_corr_histo("wjets_had");
  make_corr_histo("zjets_had");
  make_corr_histo("WVBF_had");
  make_corr_histo("ZVBF_had");
  make_corr_histo("ttbar_had");
  make_corr_histo("singletop_had");
  make_corr_histo("tWgamma_had");
  make_corr_histo("tWgamma_had");
  make_corr_histo("ttgamma_had");
  make_corr_histo("zgamma_had");
  make_leakage_histo("WVBF_sig");
  make_leakage_histo("ZVBF_sig");
  make_leakage_histo("zgamma_sig");
  make_leakage_histo("signal_sig");
  make_leakage_histo("qcd_sig");
  make_leakage_histo("WVBF_sig");
  make_leakage_histo("ZVBF_sig");
  make_leakage_histo("zgamma_sig");
//  make_plots();
}


